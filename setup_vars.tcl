
############################
#Source
############################
set tech_path "/home/arnau/docs/Tecnologia"
set top_module maxmin_top
#Definim el nom de la llibreria general
set vhdl_src_file src/maxmin_top.vhd
set vhd_libs {LOA_adder MyTypes maxmin_net_pkg maxmin_net add_accum apc clip_reg lfsr_nbits neurona_maxmin neurona_sc pcounter sinc_net unip2bip}
set tb_file tb/tb_maxmin_top_verilog.v
#Num imatges per simular
set tb_img_num 1

############################
#Floorplan
############################
set max_w_die 1000
set max_h_die 1000
set io_cell_grid 0.62
set c_margin 20


############################
#Important ports
############################
set in_clk clk
set in_en i_en
set in_rst ""


############################
#Timing constraints
############################
set is_mmmc true
# La freqüència està en Hz
# S'ha de canviar el periode del testbench si es canvia la freq
set freq [expr 150*pow(10,6)]
set env(simtime) "1000 ns"



############################
#Innovus design parameters
############################

set in_cell XMD
set out_cell YA2GSD
set PU {}
set disable_SMT {}
set keep {}
set ODC_default 16
set SR_slow {}
set E_pad {}
set E_net ""


##########
#No editar
##########
#La mida del die ha de ser múltiple del grid de les io cells per poder tancar bé l'anell.
set w_die [expr $max_w_die - ($max_w_die*100%round($io_cell_grid*100))/100.]
set h_die [expr $max_h_die - ($max_h_die*100%round($io_cell_grid*100))/100.]
set vhd_libs_string ""
#Posem els noms dels diferents fitxers de la llibreria
foreach lib $vhd_libs {
 append vhd_libs_string " src/$lib.vhd"
}
set freqMHz "[expr int($freq/pow(10,6))]MHz"

