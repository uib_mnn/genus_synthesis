# PASOS PARA SINTETIZAR

En la carpeta del proyecto crear:
- una carpeta llamada "src" con todos los vhdl
- una carpeta llamada "tb" con el testbench
- un link a la carpeta scripts que te mando (o poner directamente la carpeta en el proyecto, se puede hacer con un link para modificar los scripts y que no sea un lío con los diferentes proyectos).
- pegar el fichero llamado setup_vars.tcl.

En el usuario debemos tener una carpeta  con la tecnologia (carpeta Tecnologia). 

Luego tienes que abrir el fichero setup_vars.tcl y cambiar la ruta de la primera línia: `set tech_path "/home/arnau/docs/Tecnologia"`. La ruta tiene que apuntar allá donde hayas puesto la carpeta "Tecnologia".

Para abrir Genus entra en el directorio de tu proyecto y usa el comando "genus" o "genus -gui" si tienes curiosidad por la GUI, pero no te hace falta. 

Una vez dentro de genus llama el script general con el comando `source scripts/genus_flow.tcl`. Este script termina con el comando `exit` si quieres juguetear dentro de genus con el diseño sintetizado, comentala, pero recuerda que vamos con las licencias justas, así que ciérralo cuando termines.

Los vcd se te habrán generado automáticamente en una carpeta `sim`.

--------------------------------------------
# TROUBLESHOOT
me daba un error:
```
@file(genus_set_mmmc_constraints.tcl) 87: validate_constraints -rtl -detail -sdc sdc/* > timingReports/validate_constraints.rpt
Error   : Unable to run CCD. [ECCD-412] [validate_constraints]
        : Unable to find CCD installation in the current path. 
        : Set the environment variables VERPLEX_HOME_CCD/PATH appropriately.
```
---------------------------------------------
solucion:
-----------------------
Mira que tengas definidas las rutas de licencias y variables de entorno:

las variables de entorno se encuentran en mi usuario, en el fichero ".cshrc" que esta en home/<mi_user>

he agregado al repo el `.cshrc` con el que he compilado para futuros usos.

-----------------------------------------
# Donde mirar reports:

En la carpeta solutions hay diferentes reports. El general es el "qor"
O desde la terminal de genus usando el comando report_qor, report_area, report_timing, etc
