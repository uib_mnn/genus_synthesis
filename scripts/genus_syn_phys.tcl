source scripts/genus_constraints_phys.tcl



#Fem el placement
syn_opt -physical

write_snapshot -directory snapshot -tag 5_phsyical_opt
#summary_table -dir snapshot
report_summary -directory snapshot


check_floorplan

#Segona optimització amb l'opció incremental.
syn_opt -physical -incremental
#syn_opt -physical 

write_snapshot -directory snapshot -tag 6_physical_opt_inc
#summary_table -outdir snapshot
report_summary -directory snapshot

check_design -unresolved
check_floorplan
###################################################
# Analyze congestion and timing
###################################################

report_congestion
report_timing -physical -gui

##Update netlist

#Després haurem d'editar la netlist per afegir-hi les connexions dels io pads
#La comanda write_design té una opció per comprimir els fitxers, no la farem servir per poder editar més fàcilment la netlist

  #change_names -lec -verilog -log_changes ../common/syn_results/change_names.lec.log
  #report_timing -end > ../common/syn_results/design.endpoint.rpt
  #report_timing -gtd -num 100 > ../common/syn_results/design.gtd.rpt
  write_db -to_file syn_results/design.db
  write_hdl > syn_results/design.vg
  write_tcf > syn_results/design.tcf
  write_saif > syn_results/design.saif
  if {!$is_mmmc} {write_sdc > syn_results/design.sdc}
  if {$is_mmmc} {foreach mode {typical fast slow} {write_sdc -view *$mode > syn_results/design_$mode.sdc}}
  # No hem implementat un scan pad, perquè hem considerat que calia.
  # L'scan pad és útil per circuits seqüencials i que han de tenir tirades de molts xips, que no és el nostre cas.
  #write_scandef >  syn_results/design.scandef
  write_def >   syn_results/design.def
  write_design -innovus -tcf -base_name syn_results/design.save/design $top_module




  #Save design

  write_db -to_file solutions/top_mapped2.db
  write_hdl > solutions/top_mapped2.vg
  report_qor > solutions/top_mapped2.qor.rpt
  report_timing -physical > solutions/top_mapped2.timing_phys.rpt
  report_design_rules   > solutions/top_mapped2.edrc.rpt
  report_power -depth 0 > solutions/top_mapped2.power.rpt
  report_power -depth 2 > solutions/top_mapped2.h_power.rpt
  report_power -depth 2 -verbose > solutions/top_mapped2.h_powerv.rpt
  report_area -depth 4  > solutions/top_mapped2.area.rpt
  write_tcf > solutions/top_mapped2.tcf
  write_saif > solutions/top_mapped2.saif
  if {!$is_mmmc} {write_sdc > solutions/top_mapped2.sdc}
  if {$is_mmmc} {foreach mode {typical fast slow} {write_sdc -view *$mode > solutions/top_mapped2_$mode.sdc}}

  #write_scandef > solutions/top_mapped2.scandef 
  #write_atpg -stil > solutions/top_mapped2.stil
  #write_atpg -cadence > solutions/top_mapped2.cds_test
  #write_et_atpg -directory solutions/atpg -library "../common/libs/core_hvt_vlog/uk65lscllmvbbh.v ../common/libs/core_lvt_vlog/uk65lscllmvbbl.v ../common/libs/io_vlog/u065gioll25mvir.v ../common/macros/ASRAM/ASRAM.v ../common/macros/ROM_IM/ROM_IM.v ../common/macros/DATA_IM/DATA_IM.v" -build_model_options "definemacro=functional"
  #check_dft_rules > solutions/top_mapped2.dft_rules
  #report_dft_chains  > solutions/top_mapped2.dft_chains
  #write_encounter design  -base_name solutions/rc_enc/rc_enc -gzip_files

  ###################################################
# Save out the design (write_design & write_encounter)
###################################################
# ADD COMMANDS HERE
write_encounter design  -basename solutions/rc_enc/rc_enc -gzip_files
write_design -basename solutions/des2/design -innovus -gzip -tcf


#####################
#Guardem el nom dels ports
#####################

set input_list [get_db [all_inputs] .base_name]
regsub -all "{" $input_list "" input_list
regsub -all "}" $input_list "" input_list

set filew [open syn_results/design_inputs.txt "w"]
puts -nonewline $filew $input_list
close $filew

set output_list [get_db [all_outputs] .base_name]
regsub -all "{" $output_list "" output_list
regsub -all "}" $output_list "" output_list

set filew [open syn_results/design_outputs.txt "w"]
puts -nonewline $filew $output_list
close $filew



##############
#Guardem el disseny per optimitzar l'anàlisi de consum
##############

write_design -basename design/post_syn_phys

#exec xrun -mcl 8 +access+r -vlog_ext +.lib.src -sv -messages -input scripts/xrun.tcl -timescale 1ns/1ps design/post_syn_phys.v $tb_file -v /eda/pdk/UMC/18/TECH/verilog/fsa0a_c_generic_core_30.lib.src
exec xrun -mcl 8 +access+r -vlog_ext +.lib.src -sv -messages -input scripts/xrun.tcl -timescale 1ns/1ps design/post_syn_phys.v $tb_file -v $tech_path/verilog/fsa0a_c_generic_core_30.lib.src

#exec mv sim/dump.tcf sim/dump_post_syn_phys.tcf
exec mv sim/testpattern.vcd sim/sim_post_syn_phys.vcd


write_db -all_root_attributes -to_file save/syn_phys.db




#Guardem una copia de tots els resultats.
if {![file exists history_results]} {file mkdir history_results}
if {![file exists history_results/$top_module$freqMHz]} {file mkdir history_results/$top_module$freqMHz}
if {[file exists history_results/$top_module$freqMHz/syn]} {file delete -force history_results/$top_module$freqMHz/syn} 
file mkdir history_results/$top_module$freqMHz/syn

file copy solutions history_results/$top_module$freqMHz/syn
file copy save history_results/$top_module$freqMHz/syn
file copy sim history_results/$top_module$freqMHz/syn
file copy sdc history_results/$top_module$freqMHz/syn
file copy timingReports history_results/$top_module$freqMHz/syn
file copy syn_results history_results/$top_module$freqMHz/syn
file copy genus$freqMHz.log history_results/$top_module$freqMHz/syn


