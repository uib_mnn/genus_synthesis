#Això s'ha d'executar després d'elaborar el disseny, abans de fer el primer syn_generic, que és on desmunta tots els mòduls.

#Llista de mòduls
get_db root .modules.basename


#Llista d'instàncies del mòdul
get_db module:module_name .hinsts.basename

#Llista de pins de la instància N. Veiem que hi ha els top i els dels submòduls.
get_db hinst:inst_name .hpins

#Per fixar una instància fins al final del flow i veure com la sintetitza hem de fixar els seus pins.
set_db [get_db [get_db hinsts inst_name] .hpins] .preserve true
