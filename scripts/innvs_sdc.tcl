# Actualitzem les constraints de tempr

#El sufix $sport de les entrades està definit a edit_genus_output.tcl


#Crea un clock anomenat clk
#El period està en ns
set period [expr (1./$freq)*pow(10,9)]
create_clock [get_ports $in_clk$sport] -name clk -period $period


### Clock transiston time model for ideal mode (max=0.4, min=0.05) 
set_clock_transition -max 0.4 [get_clocks clk]
set_clock_transition -min 0.05 [get_clocks clk]

### Clock source drive/transition constraint for propagated mode (max=0.7, min=0.05)
set_input_transition -max 0.6 [get_ports $in_clk$sport] 
set_input_transition -min 0.05 [get_ports $in_clk$sport]

#########################################
### System Synchronous Input Constraints
#########################################
# Input Pad max/min delay ~= 1.5ns/0.5ns
set_input_delay  6   -max -clock clk [remove_from_collection [all_inputs] $in_clk$sport] -network_latency_included

# Smallest possible external input delay:    2.0ns
set_input_delay  2   -min -clock clk [remove_from_collection [all_inputs] $in_clk$sport] -network_latency_included

# Drive resistance of all inputs except clk
# Defined by the max tran time on the input of the IO cell.
#set_interactive_constraint_modes default_emulate_constraint_mode
#reset_driving_cell [all_inputs] 
#Traiem els drivers perquè ja hem connectat els PADS.
set_drive -max 0.18 [remove_from_collection [all_inputs] $in_clk$sport]
set_drive -min 0.01 [remove_from_collection [all_inputs] $in_clk$sport]
## or by transition time:
#set_input_transition



######################################### 
### System Synchronous Output Constraints
#########################################
# Input  Pad max/min delay ~= 1.5ns/0.5ns
# Output Pad max/min delay ~= 3.0ns/1.0ns

# WC clock-to-output delay:                  0.5ns+1.5ns (Clk delay+pad) + 0.5+3.0ns (Signal delay+pad) + 1ns Margin (setup + jitter + other) = 6.5ns
# Largest possible external output delay:    8.0ns (clock period) - 6.5ns (minimum input setup constraint) = 1.5ns
set_output_delay  1.5 -max -clock clk [all_outputs] -network_latency_included

# BC clock-to-output delay:                  0.25ns+0.5ns (Clk delay+pad) + 0.25ns+1.0ns (Signal delay+pad) - 1ns Margin (setup + jitter + other) = 1ns
# Smallest possible external output delay:   - (1ns) = -1ns
set_output_delay -1.0 -min -clock clk [all_outputs] -network_latency_included

# Define additional load capacitance in addition to the output driver pin capacitance of ~3.5pF 
set_load -max 5.00 [all_outputs]
set_load -min 0.01 [all_outputs]

#########################################
### Async reset network 
#########################################
### Async reset assertion timing is not important 
### Async reset de-assertion arrival timing is not important (since the reset synchroniser will synchronise it on chip)
if {$in_en != ""} {
 set_false_path -from [get_ports $in_en$sport]
 set_false_path -fall_through [get_nets $in_en$sport]
 set_false_path -fall_through [get_nets $in_en]
}

if {$in_rst != ""} {
 set_false_path -from [get_ports $in_en$sport]
 set_false_path -fall_through [get_nets $in_en$sport]
 set_false_path -fall_through [get_nets $in_en]
}

#########################################
# Prevent Optimisation of IO Pads
#########################################

##ATENCIÓ!!!!###
#Això no he descobert com fer-ho

#set_dont_touch [get_cells XM*]
#set_dont_touch [get_cells XM*]
# Prevent any optimisation of reset synchrnoniser/glitch filter during PnR
#set_dont_touch [get_cells system_top_inst/reset_sync]

#########################################
# Incremental propagated clock constraints
# For WC and BC timing corners
#########################################

### Clock jitter (max=0.15, min=0.1) 
set_clock_uncertainty -setup [expr 0.1*$period]  [get_clocks clk]
set_clock_uncertainty -hold  [expr 0.05*$period]  [get_clocks clk]
# Note: Lower jitter and skew at min corner

### Propagate all non-virtual clocks
#set_propagated_clock "master_clock"
set_propagated_clock [all_clocks]
# Note: This relies upon the input transition being defined for the clock net
# "set_clock_uncertainty" still applies
# "set_clock_latency -source" still applies


#########################################
# Path Groups
#########################################
# Note: These will effect the quality of optimisation
group_path -name in2reg  -from [all_inputs]
group_path -name reg2out -to   [all_outputs]
group_path -name in2out  -from [all_inputs]  -to [all_outputs] 
#group_path -name reg2reg  -from [all_registers]  -to [all_registers]
#group_path -name reg2reg  -from [all_registers -clock ...]  -to [all_registers -clock ...]

