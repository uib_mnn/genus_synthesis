
#Model de mmmc.tcl agafat de l'ajuda de Cadence
## creating library_sets


create_library_set -name wcl_slow \
    -timing { fsa0a_c_generic_core_ss1p62v125c.lib \
    foa0a_o_t33_generic_cd_io_ss1p62v125c.lib }

create_library_set -name wcl_fast \
    -timing { fsa0a_c_generic_core_ff1p98vm40c.lib \
      foa0a_o_t33_generic_cd_io_ff1p98vm40c.lib }

create_library_set -name wcl_typical \
    -timing { fsa0a_c_generic_core_tt1p8v25c.lib \
      foa0a_o_t33_generic_cd_io_tt1p8v25c.lib }

## creating operating conditions

create_opcond -name op_cond_wcl_slow -process 1.0 -voltage 1.62 -temperature 125.0

create_opcond -name op_cond_wcl_fast -process 1.0 -voltage 1.98 -temperature -40.0

create_opcond -name op_cond_wcl_typical -process 1.0 -voltage 1.8 -temperature 25.0

## creating timing_conditions

create_timing_condition -name timing_cond_wcl_slow \
    -opcond op_cond_wcl_slow \
    -library_sets { wcl_slow }

create_timing_condition -name timing_cond_wcl_fast \
    -opcond op_cond_wcl_fast \
    -library_sets { wcl_fast }

create_timing_condition -name timing_cond_wcl_typical \
    -opcond op_cond_wcl_typical \
    -library_sets { wcl_typical }


## creating rc_corners
#Com que no tenim la cap_table, la canviem i posem el QRC file
#    -cap_table typical.capTbl \

create_rc_corner -name rc_corner \
    -qrc_tech $tech_path/qrc/qrcTechFile_typ \
    -pre_route_res 1.0 \
    -pre_route_cap 1.0 \
    -pre_route_clock_res 0.0 \
    -pre_route_clock_cap 0.0 \
    -post_route_res {1.0 1.0 1.0} \
    -post_route_cap {1.0 1.0 1.0} \
    -post_route_cross_cap {1.0 1.0 1.0} \
    -post_route_clock_res {1.0 1.0 1.0} \
    -post_route_clock_cap {1.0 1.0 1.0}

## creating delay_corners

create_delay_corner -name delay_corner_wcl_slow \
    -early_timing_condition { timing_cond_wcl_slow } \
    -late_timing_condition { timing_cond_wcl_slow } \
    -early_rc_corner rc_corner \
    -late_rc_corner rc_corner

create_delay_corner -name delay_corner_wcl_fast \
    -early_timing_condition { timing_cond_wcl_fast } \
    -late_timing_condition { timing_cond_wcl_fast } \
    -early_rc_corner rc_corner \
    -late_rc_corner rc_corner

create_delay_corner -name delay_corner_wcl_typical \
    -early_timing_condition { timing_cond_wcl_typical } \
    -late_timing_condition { timing_cond_wcl_typical } \
    -early_rc_corner rc_corner \
    -late_rc_corner rc_corner


## setting constraint_modes

create_constraint_mode -name functional_wcl_slow \
    -sdc_files { ./sdc/functional_wcl_slow.sdc }

create_constraint_mode -name functional_wcl_fast \
    -sdc_files { ./sdc/functional_wcl_fast.sdc }

create_constraint_mode -name functional_wcl_typical \
    -sdc_files { ./sdc/functional_wcl_typical.sdc }

##creating analysis_views

create_analysis_view -name view_wcl_slow \
    -constraint_mode functional_wcl_slow \
    -delay_corner delay_corner_wcl_slow

create_analysis_view -name view_wcl_fast \
    -constraint_mode functional_wcl_fast \
    -delay_corner delay_corner_wcl_fast

create_analysis_view -name view_wcl_typical \
    -constraint_mode functional_wcl_typical \
    -delay_corner delay_corner_wcl_typical

## setting up analysis_views
#L'ordre és important. La primera view serà la per defecte, que serà la usada per aquelles aplicacions que no suportin multi-mode
set_analysis_view -setup { view_wcl_slow view_wcl_fast view_wcl_typical } \
                  -hold { view_wcl_slow view_wcl_fast view_wcl_typical }



