#Carreguem el hdl i les llibreries. Si fos amb verilog no hem de posar l'opció -vhdl

foreach vhd_lib $vhd_libs {
 read_hdl -vhdl -library work src/$vhd_lib.vhd
}
read_hdl -vhdl $vhdl_src_file


#Aquí encara no tenim el disseny a la GUI, però podem interactuar-hi a través del filesystem virtual intern de Genus
#vls default.architectures/

#Abans d'elaborar el disseny, li diem que no consideri errors sobre disseny incomplet (blackboxes) o si conté latches perquè l'elaboració no falli
set_db hdl_error_on_blackbox 1
set_db hdl_error_on_latch 1
set_db report_tcl_command_error 1

#Configurem els límits de loops que pot fer en elaborar el hdl.
set_db hdl_max_loop_limit 2048

#Finalment elaborem el disseny
elaborate $top_module


#Ja tenim el disseny a la GUI. Podem interactuar-hi des de la terminal a través del filesystem virtual
#vls /designs/

#Aqui podriem llegir el Power Intent file si el disseny tingues diferents dominis de voltatge
#read_power_intent -cpf -module top_module_name cpf_file

#Aquesta comanda és important per al mmmc flow
init_design


check_design -unresolved

check_design -all

syn_generic
syn_map
syn_opt

write_db -all_root_attributes -to_file save/loaded.db
