# Generem els fitxers de sortida de Innovus.

#Els bondpads que fem servir requereixen unes vies per connectar les seva capa 5 amb la capa 4 del PAD.

source scripts/innvs_insert_bondpad_vias.tcl

write_db ./saved/signoff_ta_ok.invs
report_summary -out_dir signoff_reports/summary_report

#Guardem el GDSII
#Aquí s'hi hauran d'afegir les macros



#Generem el GDS.
#Mirem si tenim el streamOut_edited.map. Si no el tenim, generem es genera una plantilla automàtica amb les cape sque Innovus està usant perquè la puguem editar. Un cop generat podem executar-la carregant el layer map editat. Es proposa eliminar (en el streamOut_edited.map) les etiquetes de la capa 5 per assignar-hi (a Virtuoso i al rulefile de Calibre LVS) les etiquetes dels ports (que ara haurien de ser a la capa 4).
if {[file exists streamOut_edited.map]} {
write_stream OutputFiles/umc180n_$top_module.gds -map_file streamOut_edited.map -lib_name DesignLib -output_macros -mode ALL
} else {
write_stream OutputFiles/umc180n_$top_module.gds -map_file streamOut_edited.map -lib_name DesignLib -output_macros -mode ALL
puts "\n\n\n  WARNING: Atenció!!! No hi ha un streamOut editat. S'ha generat un streamOut.map automàticament amb les capes de Innovus. S'han de canviar els números perquè siguin els corresponents al GDS i guardar el fitxer amb el nom 'streamOut_edited.map'. Es proposa eliminar les etiquetes de la capa 5 (per exemple) per poder-hi posar les etiquetes dels ports (a Virtuoso).\n\n\n"
}

set virtuoso_vars "top_module = \"maxmin_top\""
append virtuoso_vars "top_gds_file = 'Output_files/umc180n_$top_module.gds'"

#Escrivim les variables per a virtuoso:
set filew [open OutputFiles/virtuoso_vars.il "w"]
puts -nonewline $filew $virtuoso_vars
close $filew



#Guardem la netlist per al LVS
#Posem -phys i -update_tie_connections per assegurar-nos que hi són totes les cel·les.
write_netlist OutputFiles/lvs_netlist_$top_module.v -phys -update_tie_connections


#Guardem la netlist per al LEC
#Traiem les cel·les que només són físiques i els ports d'alimentació per comprovar només la part funcional.
write_netlist OutputFiles/lec_netlist_$top_module.v -exclude_leaf_cells -exclude_top_pg_ports "GNDO GND VCC VCC3O" -omit_floating_ports



#Escrivim les variables per a Xcelium
set xrun_csh_vars "set vhdl_src_file = $vhdl_src_file\n"
append xrun_csh_vars "vhd_libs_string = $vhd_libs_string\n"
append xrun_csh_vars "setenv simtime = $env(simtime)\n"
set filew [open OutputFiles/xrun_vars.csh "w"]
puts -nonewline $filew $xrun_csh_vars
close $filew

#Guardem la netlist per a la simulació a nivell de portes.
write_netlist OutputFiles/sim_netlist_$top_module.v


#Extracció de paràsits per a la simulació a nivell de portes
extract_rc
#Aquí haurem d'escriure una línia per cada corner
if {!$is_mmmc} {write_parasitics -spef_file rc_corner_default.$top_module.spef -rc_corner default_emulate_rc_corner}
if {!$is_mmmc} {foreach mode {slow fast typical} {write_parasitics -spef_file rc_corner_$mode.$top_module$.spef -rc_corner *$mode}}


#SDF (Standrad Delay Format) - s'ha de mirar les opcions bé, que configuram, per a generar el sdf. jo agafo el que posa l'exemple del curs (Lab 2.6 p.18)
write_sdf -process 1.0:1.0:1.0 -voltage 1.96:1.8:1.62 -temp -40:25:125 OutputFiles/$top_module.sdf

#Simulació a nivel de portes
source scripts/innvs_final_simulations.tcl

#Guardem una copia de tots els resultats.
if {[file exists history_results/$top_module$freqMHz]} {file delete -force history_results/$top_module$freqMHz/PndR} 
file mkdir history_results/$top_module$freqMHz/PndR


file copy signoff_reports history_results/$top_module$freqMHz/PndR
file copy saved history_results/$top_module$freqMHz/PndR
file copy sim history_results/$top_module$freqMHz/PndR
file copy OutputFiles history_results/$top_module$freqMHz/PndR
file copy EPA_results history_results/$top_module$freqMHz/PndR
file copy ERA_results history_results/$top_module$freqMHz/PndR
file copy PA_results history_results/$top_module$freqMHz/PndR
file copy RA_results history_results/$top_module$freqMHz/PndR
file copy middle_reports history_results/$top_module$freqMHz/PndR

time_design -sign_off -report_only -slack_report -report_dir signoff_reports



