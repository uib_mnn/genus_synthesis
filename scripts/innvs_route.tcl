#adding Core Filler Cells

#Tots els fillers
#add_fillers -base_cells FILLERCC FILLERBC FILLERAC FILLER8C FILLER8 FILLER64 FILLER4C FILLER4 FILLER32 FILLER2C FILLER2 FILLER16 FILLER1 -prefix FILLER




#Fillers sense capacitat de decoupling. Ho preferim perquè les cel·les són caixes negres i no podrem saber si ens quede capacitats a l'aire
add_fillers -base_cells FILLER8 FILLER64 FILLER4 FILLER32 FILLER2 FILLER16 FILLER1 -prefix FILLER
write_db ./saved/add_fillcore.invs
report_summary -out_file middle_reports/add_fillcore.rpt -no_html


#run nanoRoute (es fa amb la GUI, Route-> NanoRoute -> Route, a Innovus, i aquestes son les comandes que he vist surten quan es clica OK, a la consola del nanoRoute un cop configurat)
#get_multi_cpu_usage -local_cpu
#get_multi_cpu_usage -cpu_per_remote_host
#get_multi_cpu_usage -remote_host

#Fem un report dels drc abans de fer el routing i eliminem les nets erronies
#Aixo es necessari perque el corrector de drcs funcioni be.
delete_markers
check_drc -limit 10000000
delete_routes_with_violations

route_design -global_detail
#route_design -global



set_db route_design_detail_use_multi_cut_via_effort "high"
set_db route_design_antenna_cell_name "ANTENNA"
set_db route_design_concurrent_minimize_via_count_effort "high"
set_db route_design_antenna_diode_insertion true
set_db route_design_reserve_space_for_multi_cut true
set_db route_design_top_routing_layer 6
set_db route_design_with_si_driven true
set_db route_design_with_timing_driven true
set_db route_design_detail_fix_antenna true

route_global_detail
write_db ./saved/nanorouted_pre_opt.invs
report_summary -out_file middle_reports/nanorouted_pre_opt.rpt -no_html

#Aquesta comanda mou algunes cel·les i pot fer overlapping entre elles.
#opt_design -post_route
#check_place



#Fem una segona iteració de routing per si han quedat errors
delete_markers
check_drc -limit 10000000
delete_routes_with_violations
route_global_detail
check_place
write_db ./saved/nanorouted.invs
report_summary -out_file middle_reports/nanorouted.rpt -no_html

#opt_design -post_route
gui_redraw
gui_fit


#No cal fer metal fill perquè ja el fan els de UMC
#Fem els metal fill per aconseguir el minim de densitat de metall per capa especificada per la tecnologia.
#set_via_fill -layer "[get_db layers .name *via*]" -window_size 500 500 -window_step 250 250 -min_density 0.005 -max_density 30
#set_metal_fill -layer "metal1" -min_density 30 -max_density 80 -window_size 500 500 -window_step 250 250 -gap_spacing 0.28
#set_metal_fill -layer "metal2 metal3 metal4 metal5" -min_density 30 -max_density 80 -window_size 500 500 -window_step 250 250 -gap_spacing 0.32
#set_metal_fill -layer "metal6" -min_density 30 -max_density 80 -window_size 500 500 -window_step 250 250 -gap_spacing 1.5 -min_width 1.2
#add_via_fill -layer "[get_db layers .name *via*]" -mode {connectToPG connectBetweenFill}
#add_metal_fill -timing_aware sta -nets {GND}

#guardem el disseny a la carpeta innovus (lab2.6 p.5)
#write_db innovus/nanorouted_spi_slave

#Hem de configurar el QRC per posar un màxim de nuclis segons les llicències que tenim. En aquest cas 8.
set_db extract_rc_engine post_route
set_db extract_rc_effort_level signoff

#Indiquem quantes cpus usarem per al quantus (hem d'daptar-ho a les llicències disponibles).
set_db extract_rc_qrc_cmd_type partial
set_db extract_rc_qrc_cmd_file scripts/qrc_cmd_file.tcl

set_db extract_rc_coupled true
set_db extract_rc_cap_filter_mode relative_and_coupling
set_db extract_rc_coupling_cap_threshold 0.1
set_db extract_rc_total_cap_threshold 0
set_db extract_rc_relative_cap_threshold 1

#running and fixing postroute and signoff timing analysis (lab2.6 p.6), al curs com fer-ho, t'ensenya els reports directament
time_design -post_route -report_dir signoff_reports/

#Report amb extracció de paràsits
time_design -sign_off -report_dir signoff_reports/




write_db ./saved/final_routing.invs
report_summary -out_file middle_reports/final_routing.rpt -no_html

#s'han de visualitzar els reports i veure que hi ha ZERO violacions de setup i de hold, sobretot al signOff report

#a continuació es passa a la verificació del disseny
