#DRC
check_drc -out_file solutions/design.early_drc.rpt

### Calibre DRC ###
# #Els rulefiles no funcionen amb rutes relatives, però els de UMC tenen "includes" amb rutes relatives. S'han d'editar hi posar-hi el path absolut (al principi i al final del fitxer).
#Abans d'obrir el menú nmDRC s'ha de fer setup > export gds per definir les opcions de la comanda streamOut: "-outputMacros" per definir els pads. Si hi ha macros s'han d'afegir els fitxers gds amb "-merge{macro.gds}".
###

#####Early Static Power analysis.#####
# La comanda set_power_analysis_mode (generada per la GUI) és de legacy
# Amb stylus s'ha de fer get_db per a cada atribut
#####
set_db power_method static
set_db power_corner max
set_db power_write_db true
set_db power_write_static_currents true
set_db power_honor_negative_energy true
set_db power_ignore_control_signals true

#Emulate virtual metal fill
#set_db extract_rc_assume_metal_fill 1

set_power_output_dir -reset
set_power_output_dir ./EPA_results
set_default_switching_activity -reset
set_default_switching_activity -input_activity 0.2 -period 10.0
read_activity_file -reset
set_power -reset
set_dynamic_power_simulation -reset
set_pg_library_mode -cell_type techonly -extraction_tech_file $tech_path/qrc/qrcTechFile_typ -lef_layer_map $tech_path/qrc/lef2qrc.map

if {!$is_mmmc} {
set_db power_view default_emulate_view
report_power -rail_analysis_format VS -out_file ./EPA_results/default_view_$top_module.rpt
}
if {$is_mmmc} {
  foreach mode {typical fast slow} {
    set_db power_view view_wcl_$mode
    report_power -rail_analysis_format VS -out_file ./EPA_results/EPA_${mode}_$top_module.rpt
  }
}
#####Early Rail analysis.#####

set_rail_analysis_mode -method era_static -power_switch_eco false -write_movies false -save_voltage_waveforms false -write_decap_eco true -accuracy xd -process_techgen_em_rules false -enable_rlrp_analysis false -extraction_tech_file $tech_path/qrc/qrcTechFile_typ -voltage_source_search_distance 50 -ignore_shorts false -enable_manufacturing_effects false -report_via_current_direction false

#Escrivim la ubicació dels pads de VCC
write_power_pads -net VCC -auto_fetch
gui_set_draw_view place
write_power_pads -net VCC -voltage_source_file ./ERA_results/design.pp


#Realitzem el Rail Analysis
set_pg_nets -net VCC -voltage 1.62 -threshold 1.458
set_power_data -reset
set_power_data -format current -scale 1 EPA_results/static_VCC.ptiavg
set_power_pads -reset
set_power_pads -net VCC -format xy -file ./ERA_results/design.pp
set_package -reset
set_package -spice_model_file {} -mapping_file {}
set_net_group -reset
set_advanced_rail_options -reset
report_rail -type net -output_dir ./ERA_results VCC

#Per visualitzar els resultats s'ha d'anar a Power > Power&Rail results.
#S'han d'introduir els resultats clicant la casella DB Setup
#És recomanable configurar Layers/Nets posant al mínim l'opacitat del Rail Analysis i del Disseny.

write_pg_library -out_dir pgv
