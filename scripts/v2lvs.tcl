v2lvs::set_verbose_level -level 3

v2lvs::load_verilog -filename OutputFiles/lvs_netlist_maxmin_top.v
v2lvs::find_module -under maxmin_top

#Definim les pg nets.
v2lvs::override_globals -supply_not_connected -supply0 GND -supply1 VCC
v2lvs::write_output -filename ./Calibre/maxmin_top.src.net

exit

