#Editem el mode del placement
#Improves clock wirelength and subsequently clock power by adding more weight to clock nets between clock gates and their respective Flops.
set_db place_global_clock_power_driven true
#Specifies that placement is aware of clock gate cells in the design.  
set_db place_global_clock_gate_aware true
#Li diem que modeli com el clock tree per tenir en compte la congestion durant el placement
#set_db design_early_clock_flow true
#create_clock_tree_spec
#report_skew_groups
# Do not perform scan chain reordering after placement.
set_db place_global_reorder_scan false
# I/O pins are ignored during global placement and no legalization is done at the end.
set_db place_global_place_io_pins false
#Evitem que es posin cel·les sota alimentació i terra
set_db place_detail_preroute_as_obs {1 2 3 4 5 6}
set_db place_design_floorplan_mode false

#Com que per a les celles anem sobrats d'area, li diem a Innovus que augmenti la seva area en un 20%, aixo facilita reduir la congestio
#set_cell_padding -cells * -right_side 2 -bottom_side 2 -left_side 2 -top_side 2
set_db place_global_cong_effort high

#place_design
place_opt_design
#place_fix_congestion

#Fem un report del placement
check_place placeReports/design_placed.checkPlace
write_db ./saved/placed_stdcells.invs
report_summary -out_file middle_reports/placed_stdcells.rpt -no_html


# Afegim les cel·les Tie High i Low per protegir el polygate de les portes que estan permanentment a VCC o GND.
set_db add_tieoffs_max_fanout 10
set_db add_tieoffs_honor_dont_touch false
set_db add_tieoffs_create_hports false
add_tieoffs -prefix TIE -lib_cell {TIE0 TIE1}
#place_fix_congestion
write_db ./saved/add_tiehilo.invs
report_summary -out_file middle_reports/add_tiehilo.rpt -no_html

# En cas d'haver implementat un scan pad, aquí es podria executar un scan chain.

#Fem un report del placement
check_place placeReports/design_tied.checkPlace

place_opt_design -incremental
write_db ./saved/placed_incremental.invs
report_summary -out_file middle_reports/placed_incremental.rpt -no_html


# Fem un early routing per optimitzar les pistes
#route_early_global -bottom_routing_layer 1 -top_routing_layer 6 -honor_partition_pin_guide
#report_congestion
#place_fix_congestion
#Fem un informe de temps pre CTS. Per defecte només mirar els temps de setup, per mira els de hold se li hauria d'indicar amb -hold.
time_design -pre_cts

#Optimització pre CTS
set_db opt_useful_skew_max_skew false
set_db opt_useful_skew_no_boundary false
#set_db opt_useful_skew_cells "DEL* BUF*CK INV*CK"
set_db opt_useful_skew_max_allowed_delay 1
set_db opt_effort high
set_db opt_power_effort none
set_db opt_area_recovery true
set_db opt_remove_redundant_insts true
set_db opt_all_end_points true
set_db opt_setup_target_slack 0.1
set_db opt_hold_target_slack 0.5
set_db opt_max_density 0.95
set_db opt_drv_margin 0
set_db opt_useful_skew true
opt_design -pre_cts
write_db ./saved/preCTS_setup_opt.invs
report_summary -out_file middle_reports/preCTS_setup_opt.rpt -no_html

#Si encara hi ha DRV violations fer
opt_design -pre_cts -drv
#report_congestion
#place_fix_congestion
write_db ./saved/preCTS_DRV_setup_opt.invs
report_summary -out_file middle_reports/preCTS_DRV_setup_opt.rpt -no_html


