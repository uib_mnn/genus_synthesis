#dumptcf -output sim/dump.tcf -dumpwireasnet -overwrite
set pack_assert_off { std_logic_arith numeric_std }
database -open vcddb -vcd -into sim/testpattern.vcd -default -timescale ps
probe -create -database vcddb -depth all -all 
#run 280000 ns
if {$env(simtime) == ""} { run } else {run $env(simtime)}
#dumptcf -end
exit
