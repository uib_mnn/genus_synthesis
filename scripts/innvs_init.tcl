#S'ha d'executar l'innovus amb l'opció -stylus si estem a la versió 17
#innovus -stylus



#Definim la tecnologia en nanòmetres. Ho comentem perquè ja l'hem definida abans al Genus.
#set_db design_process_node 180
#set_db init_min_dbu_per_micron 1000


set write_def_hierarchy_delimiter /

set_db init_power_nets {VCC VCC3O}
set_db init_ground_nets {GND GNDO}
#set_db init_power_nets VCC
#set_db init_ground_nets GND

#Per poder implementar les constraints del clock
set_db timing_allow_input_delay_on_clock_source true

#Carreguem el dissen sintetitzat a Genus
source syn_results/design.save/design_edited.invs_setup.tcl
#Retirem tots els objectes que hi ha del floorplan de genus
unplace_obj -all
delete_routes
#Definim el Grid
# set user grids
set_preference ConstraintUserXGrid 0.01
set_preference ConstraintUserXOffset 0.01
set_preference ConstraintUserYGrid 0.01
set_preference ConstraintUserYOffset 0.01
set_preference SnapAllCorners 1


#Definim el mapa entre LEF i QRCtech file per permetre l'extracció de paràsits.
set_db extract_rc_lef_tech_file_map $tech_path/qrc/lef2qrc.map


#Definim el mode d'anàlisis de temps
set_db timing_analysis_type ocv
set_db timing_analysis_cppr both
set_db timing_analysis_useful_skew true


#Definim el mode d'optimització
set_db opt_fix_fanout_load true
set_db opt_all_end_points true
set_db opt_useful_skew true


#Escrivim la posició dels pins
#write_io_file -locations spi_slave.save.io

write_db ./saved/import_design.invs
report_summary -out_file middle_reports/import_design.rpt -no_html

#Check the time report before placement
time_design -pre_place -expanded_views


#Ajustem el floorplan fent-ne un de nou
floorplan_set_snap_rule -for DIE -grid MG -force
create_floorplan -die_size "$w_die $h_die $c_margin $c_margin $c_margin $c_margin" -core_margins_by io -floorplan_origin  l -flip s
write_db ./saved/floorplaned.invs
report_summary -out_file middle_reports/floorplaned.rpt -no_html


#Fitxer dels PADs generat per innvs_setup.tcl
read_io_file $top_module.default.io -no_die_size_adjust


#Connectem els pins dels PADs per configurar-los (Pull-down, SR, ST,...)
source syn_results/design.save/innvs_PAD_conf.tcl
write_db ./saved/ioplaced.invs
report_summary -out_file middle_reports/ioplaced.rpt -no_html


#snap floorplan before adding IO filler cells (Lab2.2 p.5)
snap_floorplan -guide -block -std_cell -io_pad -pin -pin_guide -route_blockage -pin_blockage -partition_core -place_blockage -macro_pin


#Aquí afegiríem les macros
#write_db ./saved/blks_placed.invs

#Aquí bloquegem el routing prop de les macros. Mirar els valors que s'haurien de posar entre {}
#select_inst nom_de_la_inst
#create_place_halo {} nom_de_la_inst
#write_db ./saved/blks_halo.invs


#Connectem les xarxes globals
connect_global_net VCC -type pg_pin -pin_base_name VCC -auto_tie -all -verbose
connect_global_net VCC3O -type pg_pin -pin_base_name VCC3O -auto_tie -all -verbose
connect_global_net GND -type pg_pin -pin_base_name GND -auto_tie -all -verbose
connect_global_net GNDO -type pg_pin -pin_base_name GNDO -auto_tie -all -verbose
write_db ./saved/globalnetconn.invs
report_summary -out_file middle_reports/globalnetconn.rpt -no_html



#Posem els anells
add_rings -nets {VCC GND} -type core_rings -center 1 -layer {top M6 bottom M6 right M5 left M5} -width 7 -spacing 1.5 
write_db ./saved/add_corerings.invs
report_summary -out_file middle_reports/add_corerings.rpt -no_html

#Aquí afegíriem els anells de les macros

#Afegim power stripes
#add_stripes -block_ring_top_layer_limit metal6 -max_same_layer_jog_length 4 -pad_core_ring_bottom_layer_limit metal5 -set_to_set_distance 80 -pad_core_ring_top_layer_limit metal6 -spacing 3.1 -start_offset 50 -merge_stripes_value 0.1 -layer metal6 -block_ring_bottom_layer_limit metal5 -width 5.0 -nets {VCC GND}
#write_db /saved/add_stripes.invs


#Posem les línies d'alimentació/terra
route_special -connect {block_pin pad_pin pad_ring core_pin floating_stripe} -layer_change_range { M1(1) M6(6) } -block_pin_target {nearest_target} -pad_pin_port_connect {all_port one_geom} -pad_pin_target {nearest_target} -core_pin_target {first_after_row_end} -floating_stripe_target {block_ring pad_ring ring stripe ring_pin block_pin followpin} -allow_jogging 1 -crossover_via_layer_range { M1(1) M6(6) } -nets { GND VCC } -allow_layer_change 1 -block_pin use_lef -target_via_layer_range { M1(1) M6(6) }
write_db ./saved/fplan_final.invs
report_summary -out_file middle_reports/fplan_final.rpt -no_html

gui_fit
