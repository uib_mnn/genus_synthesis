
# Tcl per editar els fitxers de sortida de Genus. Generem fitxers nous amb un sufix per conservar els originals

# 1. Generem un default io file amb la informacio dels PADs
# 2. Editem el design.v: canviem el nom de les input i output nets per desconnectar-les dels wires que van al core. A les entrades i sortides els donarem un nom diferent (hi afegirem un sufix). Afegim la netlist dels PADs connectats a les nets pertinents (incloent-hi les configuracions pull-up,...).
# 3. Editem el nom dels ports al design.wnm_attrs.tcl perquè sigui coherent amb el punt 2.
# 4. Editem el nom dels ports al design.default_emulate_constraint_mode.sdc
# 5. Editem els fitxers design.invs_setup.tcl, design.invs_init.tcl i design.mmmc.tcl perquè criden els fitxers anteriors.
# 6. Generem un nou fitxer innvs_PAD_conf.tcl per configurar els PADs en model pull-down, etc..

# Si editem a mà algun dels fitxers que escriu aquesta script, podria deixar de funcionar com s'espera.

#Les variables a nivell d'usuari d'aquest script han d'haver estat definides al fitxer setup_vars.tcl

################
###Text per editar
################

#Nom del fitxer_netlist, del d'atributs i del tcl per configurar els pins
set path syn_results/design.save/
set designv design.v
set designattrs design.wnm_attrs.tcl
if {!$is_mmmc} {set designsdc design.default_emulate_constraint_mode.sdc}
if {$is_mmmc} {set designsdc {design.functional_wcl_fast.sdc design.functional_wcl_slow.sdc design.functional_wcl_typical.sdc}}
set designsp design.invs_setup.tcl
set designinit design.invs_init.tcl
set designmmmc design.mmmc.tcl
set padconf innvs_PAD_conf.tcl
set designio $top_module.default.io


# Per defecte connectarem tots els enables dels PADs de sortida a VCC. Aquí es poden indicar les nets de quins PADs es volen connectar a una altra net i la net_enable en qüestió. Podem indicar o_data_parallel per a tot el bus


#####################
###Text fix
#####################


#Sufix per a les noves entrades i sortides.
set sport "_pt"
#Sufix per a les instàncies dels PADs.
set sinst "_inst"

#Llista d'entrades i sortides per editar
set fp [open "syn_results/design_inputs.txt" r]
set in_list [read $fp]
close $fp


set in_pinbus_list ""
set in_list_new ""
set in_bus_list ""
set in_inst_list ""
foreach in $in_list {
	if {[string match {*\[*} $in]} {
		append in_list_new " "
		append in_list_new [regsub {\[} $in "$sport\["]
		set bus_name [lindex [regsub -all {[][]} $in " "] 0]
	        set busbitnum [lindex [regsub -all {[][]} $in " "] 1]
		append in_inst_list " $bus_name$busbitnum$sinst"
		if {"$bus_name" in $in_bus_list} {} else {append in_bus_list " $bus_name"}
		if {"$bus_name" in $in_pinbus_list} {} else {append in_pinbus_list " $bus_name"}
	} else {
		append in_list_new " "
		append in_list_new  [regsub $in $in $in$sport]
		append in_inst_list " $in$sinst"
		append in_pinbus_list " $in"
	}
}


set fp [open "syn_results/design_outputs.txt" r]
set out_list [read $fp]
close $fp

set out_pinbus_list ""
set out_list_new ""
set out_bus_list ""
set out_inst_list ""
foreach out $out_list {
	if {[string match {*\[*} $out]} {
		append out_list_new " "
		append out_list_new [regsub {\[} $out "$sport\["]
		set bus_name [lindex [regsub -all {[][]} $out " "] 0]
	        set busbitnum [lindex [regsub -all {[][]} $out " "] 1]
		append out_inst_list " $bus_name$busbitnum$sinst"
		if {"$bus_name" in $out_bus_list} {} else {append out_bus_list " $bus_name"}
		if {"$bus_name" in $out_pinbus_list} {} else {append out_pinbus_list " $bus_name"}
	} else {
		append out_list_new " "
		append out_list_new  [regsub $out $out $out$sport]
		append out_inst_list " $out$sinst"
		append out_pinbus_list " $out"
	}
}

set port_list "$in_list $out_list"
set port_list_new "$in_list_new $out_list_new"
set bus_list "$in_bus_list $out_bus_list"
set pinbus_list "$in_pinbus_list $out_pinbus_list"
set inst_list "$in_inst_list $out_inst_list"

#Sufix per als fitxers
set sfx "_edited"

#Llista de fitxers que criden a fitxers editats
set file_list "$designsp $designinit $designmmmc"
##Llista de fitxers editats
set file_name_list "$designv $designattrs $designsdc $designsp $designinit $designmmmc"

#####################
###IO file
#####################


#Spacing between pads in um
set pad_min_spacing 90
set grd_spacing 0
#Segons la documentacio el guard ring fa 16 vegades el grid
set gr_width 3.72
set pad_width 62.62
set bp_width 47
set pad_height 140.12



#set power_cells {EMPTYGRD VCCKD GNDKD EMPTYGRD VCC3IOD GNDIOD EMPTYGRD}
set power_cells_1_8 {VCCKD GNDKD}
set power_cells_3_3 {VCC3IOD GNDIOD}
set power_cells "$power_cells_1_8 $power_cells_3_3"
set ppad_top "vdd gnd vcc3iod gndiod"
set ppad_bot "vdd2 gnd2 vcc3iod2 gndiod2"
set extra_gnd_pads ""

#Ens assegurem que el nombre de PADs sigui multiple de 4. Si no ho es afegim GNDs
#COmencem a i=3 perque ja hi ha 2 pads de gnd posats.
for {set i 3} {$i<[expr (4 - [llength "$port_list $ppad_top $ppad_bot"]%4 ) + 3 ]} {incr i} {append extra_gnd_pads "gnd$i "}
set PADnum [llength "$port_list $ppad_top $ppad_bot $extra_gnd_pads"]
set PADsxSide [expr $PADnum/4]

set pad_pitch [expr ($w_die - 2*$pad_height)/$PADsxSide]
if { $pad_pitch < $pad_min_spacing } {
	puts "\n\n\n\t\t\t###################\n\t\tEl pitch dels PADs és més petit de 90 um.\n\t\t\t###################"
	exit
}
#Espai abans i despres de cada PAD que s'ha d'omplir amb fillers
set sp_for_fillers [expr ($pad_pitch-2*$gr_width-$pad_width)/2]
set filler_grids_for_sp [expr $sp_for_fillers/$io_cell_grid]




set grids_left $filler_grids_for_sp
#Required number of each kind of filler
set fillnumlist ""
set i 0
foreach num {16 8 4 2} {
  set fillnum "[expr int($grids_left/$num)] "
  set grids_left [expr $grids_left-$fillnum*$num]
  if {$grids_left == 2 || $grids_left == 1} {
    set fillnum [expr $fillnum-1]
    set grids_left [expr $grids_left + $num]
  }
  append fillnumlist " $fillnum"
}
append fillnumlist " [expr int($grids_left)]"


set offset ""

set data_out_iofile "#Default IO file\n#Versió generada amb innvs_setup_inputs.tcl\n\n"

append data_out_iofile "(globals\n\tversion = 3\n\tio_order = default\n)\n"
append data_out_iofile "(iopad\n"
foreach corn {topright bottomright bottomleft topleft} var {1 2 3 4} orientation {R0 R270 R180 R90} {append data_out_iofile "\t($corn\n\t\t(inst name=\"CORNER$var\" cell=\"CORNERD\" orientation=$orientation)\n\t)\n"}

#Variable per indicar els pads que hem posat a cada costat
set padnumber 0
#Índex per apuntar a la llista de PADs (excepte els d'alimentacio obligatoris)
set p_index 0

#A cada costat posem PADnum/4 pads. Si no són múltiples de 4, els que sobrin els posarem a l'últim costat.
set gr_count 0
set fill_count 0
set bp_count 0
set side_count 1
foreach die_side {top left bottom right} orientation {R180 R270 R0 R90} bp_orientation {R270 R0 R90 R180} {
  append data_out_iofile "\n\t($die_side\n"
  set offset [expr $pad_height + ($pad_pitch - $pad_width)/2]

  if {[string match $die_side "top"] || [string match $die_side "bottom"]} {
    set ppadlist ""
    if {[string match $die_side "top"]} {set ppadlist $ppad_top} else {set ppadlist $ppad_bot}

    foreach ppad $ppadlist pcell $power_cells {
      
      set filloffset $sp_for_fillers
      foreach numcell {16 8 4 2 1} num $fillnumlist {
	for {set i 0} {$i < $num} {incr i} {
          append data_out_iofile "\t\t(inst name=\"io_filler$fill_count\" cell=\"EMPTY${numcell}D\" orientation=$orientation offset=[expr $offset - $gr_width - $filloffset])\n"
          incr fill_count
          set filloffset [expr $filloffset - $numcell*$io_cell_grid]
        }
      }

      append data_out_iofile "\t\t(inst name=\"guard_ring$gr_count\" cell=\"EMPTYGRD\" orientation=$orientation offset=[expr $offset - $gr_width])\n"
      incr gr_count

      append data_out_iofile "\t\t(inst name=\"bondpad$bp_count\" cell=\"PADPOC6MD\" orientation=$bp_orientation offset=[expr $offset + ($pad_width - $bp_width)/2])\n"
      incr bp_count
      append data_out_iofile "\t\t(inst name=\"$ppad\" cell=\"$pcell\" orientation=$orientation offset=$offset)\n"
      set offset [expr $offset + $pad_pitch]

      append data_out_iofile "\t\t(inst name=\"guard_ring$gr_count\" cell=\"EMPTYGRD\" orientation=$orientation offset=[expr $offset - $pad_pitch + $pad_width])\n"
      incr gr_count
      incr padnumber

      set fillofset 0
      foreach numcell {16 8 4 2 1} num $fillnumlist {
	for {set i 0} {$i < $num} {incr i} {
          append data_out_iofile "\t\t(inst name=\"io_filler$fill_count\" cell=\"EMPTY${numcell}D\" orientation=$orientation offset=[expr $offset - $pad_pitch + $pad_width + $gr_width + $filloffset])\n"
          incr fill_count
          set filloffset [expr $filloffset + $numcell*$io_cell_grid]
        }
      }
    }
  }
  
  set pad_sublist [lrange "$inst_list $extra_gnd_pads" $p_index [expr $p_index + $PADsxSide*$side_count - $padnumber - 1]]

  foreach pad $pad_sublist {
    if {$pad in $in_inst_list} {set celltype $in_cell
    } elseif {$pad in $out_inst_list} {set celltype $out_cell
    } elseif {$pad in $extra_gnd_pads} {
	set celltype "GNDKD"
    }

      set filloffset $sp_for_fillers
      foreach numcell {16 8 4 2 1} num $fillnumlist {
	for {set i 0} {$i < $num} {incr i} {
          append data_out_iofile "\t\t(inst name=\"io_filler$fill_count\" cell=\"EMPTY${numcell}D\" orientation=$orientation offset=[expr $offset - $gr_width - $filloffset])\n"
          incr fill_count
          set filloffset [expr $filloffset - $numcell*$io_cell_grid]
        }
      }
    append data_out_iofile "\t\t(inst name=\"guard_ring$gr_count\" cell=\"EMPTYGRD\" orientation=$orientation offset=[expr $offset - $gr_width])\n"
    incr gr_count

    append data_out_iofile "\t\t(inst name=\"bondpad$bp_count\" cell=\"PADPOC6MD\" orientation=$bp_orientation offset=[expr $offset + ($pad_width - $bp_width)/2])\n"
    incr bp_count
    append data_out_iofile "\t\t(inst name=\"$pad\" cell=\"$celltype\" orientation=$orientation offset=$offset)\n"
    incr p_index
    set offset [expr $offset + $pad_pitch]

    append data_out_iofile "\t\t(inst name=\"guard_ring$gr_count\" cell=\"EMPTYGRD\" orientation=$orientation offset=[expr $offset -$pad_pitch + $pad_width])\n"
    incr gr_count
    incr padnumber
      

      set fillofset 0
      foreach numcell {16 8 4 2 1} num $fillnumlist {
	for {set i 0} {$i < $num} {incr i} {
          append data_out_iofile "\t\t(inst name=\"io_filler$fill_count\" cell=\"EMPTY${numcell}D\" orientation=$orientation offset=[expr $offset - $pad_pitch + $pad_width + $gr_width + $filloffset])\n"
          incr fill_count
          set filloffset [expr $filloffset + $numcell*$io_cell_grid]
        }
      }
 
  }
  append data_out_iofile "\t)\n"
  incr side_count
}
append data_out_iofile ")\n"

set filew [open $designio "w"]
puts -nonewline $filew $data_out_iofile
close $filew


###############
###### design.v
###############


# Llegim el document i el separem en una matriu de línies
set fp [open "$path$designv" r]
set file_data [split [read $fp] "\n"]
close $fp

set data_out "\n\n  //Fitxer modificat automàticament des de innvs_setup_inputs.tcl per incloure la netlist dels PADs, millor no editar manualment.\n\n"


#Mirem quina és la primera paraula de cada línia
#Per defecte, excrivim la línia que llegim.
#Línies que s'han de modificar: module, input i output
#Si trobem "endmodule" deixem de llegir per introduir-hi els PADs abans
#No llegim les línies buides.

#Definició dels ports al module()
set mpstr ""
foreach port $pinbus_list {append mpstr "$port$sport, "}
regsub {,([^,]+)$} $mpstr {\1} mpstr

#Flag per escriure línies a default
set w 1
#Flag per indicar si estem llegint el top module
set tmm 0

foreach line $file_data {
  switch [lindex $line 0] {
    "module" {
      if {"$top_module" in [lindex [split "$line" "("] 0]} {
	append data_out "\n\nmodule $top_module ($mpstr);\n\n"
	set w 0
	set tmm 1
      } else {
	append data_out "$line \n"
	set w 1
      }
    }
    "input" -
    "output" {
	if {$tmm} {
	  regsub -all $sport $line "" line
	  foreach port "$pinbus_list" {regsub $port $line $port$sport line}
	  append data_out "$line\n"
	  set w 1
	} else {append data_out "$line \n"}
    }
    "endmodule" {
	if {$tmm} {break} else {append data_out "$line \n"}
    }
    default {if {$w} {append data_out "$line \n"}}
  }
}
# Afegim la netlist dels PADs

append data_out "\n\n//PAD netlist \n//Només definim les cel·les i les instàncies. Les connexions dels pins es fan a través d'innovus. Aquest codi s'ha d'afegir dins del mòdul del design.v generat per Genus.\n"

append data_out "\n//Entrades\n"
foreach in $in_list inst $in_inst_list newin $in_list_new {append data_out "  $in_cell $inst (.I($newin), .O($in));\n"}

append data_out "\n//Sortides\n"
foreach out $out_list inst $out_inst_list newout $out_list_new {append data_out "  $out_cell $inst (.I($out), .O($newout));\n"} 

append data_out "\n\nendmodule\n\n"

regsub "design" $designv "design$sfx" designv_new
set filew [open $path$designv_new "w"]
puts -nonewline $filew $data_out
close $filew


###############
###### design.wnm_attrs.tcl
###############

set data_out_attrs "\n\n  ##Fitxer modificat automàticament des de innvs_setup_inputs.tcl perquè els noms dels ports siguin coherents amb l anova netlist. Millor no editar manualment.\n\n"

#Llegim el document original perquè els pins de les altres nets s'escriuran igual.
set fp [open "$path$designattrs" r]
set file_data [read $fp]
close $fp


foreach port "$pinbus_list" {regsub -all "port:$top_module/$port" $file_data "port:$top_module/$port$sport" file_data}
append data_out_attrs $file_data


regsub "design" $designattrs "design$sfx" designattrs_new
set filew [open $path$designattrs_new "w"]
puts -nonewline $filew $data_out_attrs
close $filew


###############
###### design.xxxx.sdc
###############

# Fitxer de constraints
# Fem un bucle per si n'hi ha més d'un (en el cas de mmmc)

foreach mode_sdc_file $designsdc {

  set data_out_sdc "\n\n  ##Fitxer modificat automàticament des de innvs_setup_inputs.tcl. Millor no editar manualment.\n\n"

  #Llegim el document original.
  set fp [open "$path$mode_sdc_file" r]
  set file_data [read $fp]
  close $fp

  foreach port "$pinbus_list" {regsub -all "get_ports $port" $file_data "get_ports $port$sport" file_data}
  foreach port "$pinbus_list" {regsub -all "get_ports \{$port" $file_data "get_ports \{$port$sport" file_data}
  append data_out_sdc $file_data

  regsub "design" $mode_sdc_file "design$sfx" mode_sdc_file_new
  set filew [open $path$mode_sdc_file_new "w"]
  puts -nonewline $filew $data_out_sdc
  close $filew
}


###############
###### Altres fitxers
###############

#Canviem els noms dels fitxers que hem editat en els scripts que els fan servir.

#Com que la string design.v existeix dins de design.view i no hem editat el design.view, al final rectifiquem els canvis no desitjats.

foreach fname $file_list {
  set data_outx "\n\n  ##Fitxer modificat automàticament des de innvs_setup_inputs.tcl. Millor no editar manualment.\n\n"
  set fp [open "$path$fname" r]
  set file_data [read $fp]
  close $fp

  foreach name $file_name_list {
    regsub "design" $name "design$sfx" namesfx
    regsub -all $name $file_data $namesfx file_data
  }
  regsub -all "design_edited.view" $file_data "design.view" file_data

  append data_outx $file_data
  regsub "design" $fname "design$sfx" fnamesfx
  set filew [open $path$fnamesfx "w"]
  puts -nonewline $filew $data_outx
  close $filew
}


###############
###### innvs_PAD_conf.tcl
###############

# Fitxer per configurar els pins. Connectem les diferents terminals físiques dels PADS a les nets de VCC o GND segons la configuració que vulguem.

#connect_pin -inst instName -net netName -pin pinName

set data_out4 "\n\n  #Aquest fitxer s'ha generat automàticament des de innvs_setup_inputs.tcl, millor no editar manualment.\n\n"

# Definim els pins
set PU_net ""
set PD_net ""
set SMT_net ""


#Connectem les entrades
foreach in $in_list inst $in_inst_list {
  if {[lsearch $PU $in] >= 0 } {
    set PU_net "VCC"
    set PD_net "GND"
  } elseif {[lsearch $keep $in] >= 0 } {
    set PU_net "VCC"
    set PD_net "VCC"
  } else {
    set PU_net "GND"
    set PD_net "VCC"
  }
  if {$in in $disable_SMT} {set SMT_net "GND"} else {set SMT_net "VCC"}

  append data_out4 "connect_pin -inst $inst -net $PU_net -pin PU\n"
  append data_out4 "connect_pin -inst $inst -net $PD_net -pin PD\n"
  append data_out4 "connect_pin -inst $inst -net $SMT_net -pin SMT\n"
}

append data_out4 "\n\n"

#Connectem les sortides
#Els pins E2, E4 i E8 configuren la driving output capacity com indica al document FOA0A_O_T33_GENERIC_CD_IO_DB_v1.0.pdf pg 28.

set E2_net ""
set E4_net ""
set E8_net ""
set SR_net ""


if {[expr $ODC_default % 4 ] == 0 } {set E2_net VCC} else {set E2_net GND}
if {[lsearch {2 4 10 12} $ODC_default] >= 0} {set E4_net GND} else {set E4_net VCC}
if {$ODC_default < 10} {set E8_net GND} else {set E8_net VCC}

foreach out $out_list inst $out_inst_list {
  append data_out4 "connect_pin -inst $inst -net $E2_net -pin E2\n"
  append data_out4 "connect_pin -inst $inst -net $E4_net -pin E4\n"
  append data_out4 "connect_pin -inst $inst -net $E8_net -pin E8\n"

  if {($out in $SR_slow) || ([lindex [regsub -all {[][]} $out " "] 0] in $SR_slow)} {set SR_net VCC} else {set SR_net GND}
    append data_out4 "connect_pin -inst $inst -net $SR_net -pin SR\n"

  if {($out in $E_pad) || ([lindex [regsub -all {[][]} $out " "] 0] in $E_pad)} {
	append data_out4 "connect_pin -inst $inst -net $E_net -pin E\n"
  } else {append data_out4 "connect_pin -inst $inst -net VCC -pin E\n"
  }
}

set filew [open $path$padconf "w"]
puts -nonewline $filew $data_out4
close $filew

