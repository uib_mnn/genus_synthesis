#Flow per al P&R d'Innovus.


####### La carpeta de cada projecte ha d'incloure: #######
#1. Una carpeta src amb tots els .vhd (top i altres)
#2. Una carpeta tb amb el testbench
#3. Una carpeta (o enllaç) scripts amb els scripts.
#4. Una carpeta (o enllaç) anomenada syn_results, generada per Genus, que conté el disseny sintetitzat.
#4. Un fitxer setup_vars.tcl on s'hi han de definir les següents variables:
#5. Un fitxer streamOut.map amb la traducció de les capes de cadence a gds.

###### Paràmetres comuns Genus/Innovus: #######
#top_module: top module name
#w_die: Ample del die
#h_die: Alçada del die
#c_margin: Marge entre el core i el die
#in_clk: clock port (in RTL)
#in_en: enable port (in RTL)
#in_rst: reset port (in RTL)
#is_mmmc: true o false, indica si fem un Mult-Mode Multi-Corner Analysis
#freq: Clock frequency (Hz)
#simtime: El temps de simulacio ha de ser una var d'entorn per passar-la a xcelium. En ns.


###### Parametres exclusius Innovus: ######
#in_cell: input PAD cell
#out_cell: output PAD cell
#PU: entrades en pull-up (per defecte estaran en pull-down. Els busos s'indiquen en bloc).
#disable_SMT: entrades sense Schmitt Triger (per defecte en tindran. Els busos s'indiquen en bloc).
#keep: entrades amb el keep activat (per defecte no el tindran. Els busos s'indiquen en bloc).
#ODC_default:  Definim la Output Driving Capability per defecte dels PADs de sortida en mA. Poden ser valors enters i parells del 2 a 16.
#SR_slow: sortides amb slow SR (per defecte és Fast. Els busos s'indiquen en bloc).
#E_pad: Indiquem les sortides els enables dels PADs dels quals volem connectar a una net diferent de VCC (que és la que hi haurà per defecte).
#E_net: net enable a la que connectarem els PADs anteriors.

source setup_vars.tcl

#set_multi_cpu_usage -local_cpu 8
set_multi_cpu_usage -local_cpu 16


##################
#Flow
##################

#Guardem el moment d'inici del flow
set flow_start_time [clock seconds]

#Carreguem el menú Calibre (Siemens) perquè aparegui a la barra de la GUI
#Els rulefiles no funcionen amb rutes relatives, però els de UMC tenen "includes" amb rutes relatives. S'han d'editar hi posar-hi el path absolut (al principi i al final del fitxer).
#Abans d'obrir el menú nmDRC s'ha de fer setup > export gds per definir les opcions de la comanda streamOut: "-outputMacros" per definir els pads. Si hi ha macros s'han d'afegir els fitxers gds amb "-merge{macro.gds}".
#source $::env(CALIBRE_HOME)/lib/cal_enc.tcl

#Per recuperar el disseny en algun punt concret del flow: read_db save/fitxer.invs


#Aquest script inclou noms de variables d'entrada i sortida, mòdul, etc que s'ha de canviar per a cada disseny
source scripts/innvs_setup_inputs.tcl

#Carreguem el disseny sintetitzat a Genus
source scripts/innvs_init.tcl

#Early DRC, Power Analysis i Rail Analysis
source scripts/innvs_early_verification.tcl

#Placement
source scripts/innvs_placement.tcl

#CTS
source scripts/innvs_cts.tcl

#Routing
source scripts/innvs_route.tcl

#Verification
source scripts/innvs_verify.tcl

#Generem els fitxers de sortida de l'Innovus
source scripts/innvs_outputs.tcl

puts "\n\n\t\t\t#########################################\n\n\t\t\tFinal del flow\n\n\t\t\t#########################################\n\n\n"

#Guardem el moment final del flow i mostrem la durada
set flow_end_time [clock seconds]

puts "El contingut del fitxer setup_vars.tcl es:\n"
puts [read [open setup_vars.tcl r]]

puts "\n\nEl flow ha començat [clock format $flow_start_time -format {el %D a les %H:%M:%S}] i ha acabat [clock format $flow_end_time -format {el %D a les %H:%M:%S}].\n\n"


file copy [get_db log_file] history_results/$top_module$freqMHz/PndR

#exit



