#Posem el max nivell d'info que retornen les comandes
set_db information_level 9

#Definim la tecnologia
set_db design_process_node 180


#Mirem si existeis una carpeta on guardearem una copia dels resultats finals
if {![file exists history_results]} {file mkdir history_results}

#Definim el path de les llibreries lib i lef
set_db init_lib_search_path "$tech_path/lib $tech_path/lef"

#Definim les llibreries lib dels diferents corners enc as de no MMMC
if {!$is_mmmc} {set_db library {fsa0a_c_generic_core_ss1p62v125c.lib fsa0a_c_generic_core_ff1p98vm40c.lib fsa0a_c_generic_core_tt1p8v25c.lib foa0a_o_t33_generic_cd_io_ss1p62v125c.lib foa0a_o_t33_generic_cd_io_ff1p98vm40c.lib foa0a_o_t33_generic_cd_io_tt1p8v25c.lib}}

#Creem uns fitxers sdc dummy per poder llegir el mmmc
if {![file exists sdc]} {file mkdir sdc}
if {![file exists sdc/functional_wcl_slow.sdc]&& $is_mmmc} {close [open sdc/functional_wcl_slow.sdc w]}
if {![file exists sdc/functional_wcl_fast.sdc] && $is_mmmc} {close [open sdc/functional_wcl_fast.sdc w]}
if {![file exists sdc/functional_wcl_typical.sdc] && $is_mmmc} {close [open sdc/functional_wcl_typical.sdc w]}


#Creem els corners amb els respectius modes i views
if {$is_mmmc} {read_mmmc -design $top_module scripts/genus_mmmc.tcl}

#Llegim les llibreries físiques LEF
set_db lef_library "header6_V55_20ka.lef fsa0a_c_generic_core.lef FSA0A_C_GENERIC_CORE_ANT_V55.6.lef foa0a_o_t33_generic_cd_io.6.lef FOA0A_O_T33_GENERIC_CD_IO_ANT_V55.6.lef"

#Llegim el fitxer d'extracció de paràmetres: QRC technology file.
#Aquesta comanda només s'executa si no hi ha mmmc
if {!$is_mmmc} {read_qrc $tech_path/qrc/qrcTechFile_typ}


######
#Afegim les Cap tables (models de capacitat)
######
#No utilitzem Cap Tables perquè en principi la informació ja va inclosa en la lef file. Es pot comprovar amb la comanda report_ple
#set_db cap_table_file "/eda/pdk/UMC/18/TECH/capTbl/basic.capTbl"
get_db operating_conditions /

set_db syn_generic_effort medium
set_db syn_map_effort medium
set_db syn_opt_effort medium

#Activem que optimitzi tots els camins amb slack negatiu enlloc de només el pitjor
set_db tns_opto true

#Inhabilitem que implementi clock gating durant l'optimització
set_db lp_insert_clock_gating false
