
#miram que tot el design segueix OK
check_design -all

#Read/write switching activity
#Hem d'esborrar la carpeta que pugui haver auto-generat xcelium anteriorment
if {[file exists xcelim.d]} {rm -r xcelium.d}
exec xrun -mcl 8 +access+r -sv -v93 -messages $vhdl_src_file $tb_file -input scripts/xrun.tcl -makelib work $vhd_libs_string -endlib


#mv sim/dump.tcf sim/dump_pre_syn.tcf
mv sim/testpattern.vcd sim/sim_pre_syn.vcd

#Llegim la switching activity per optimitzar la síntesi
#read_tcf sim/dump_pre_syn.tcf
read_vcd -static sim/sim_pre_syn.vcd

#It is useful to track the synthesis progress
write_snapshot -outdir snapshot -tag import
#report_summary -directory snapshot

#Abans de mapejar el disseny hem d'habilitar el fixing of constant output ports and multioutput ports. Això habilita fixar nets que carreguen múltiples output ports durant l'optimització
set_db remove_assigns true
set_remove_assign_options -verbose
add_assign_buffer_options -verbose

#Abans d'optimitzar, evita que l'eina faci canvis a la jerarquia. Síntesis d'alt esforç (syn_map) faria un ungroup automàtic per aconseguir millors resultats però faria més difícil d'entendre la jerarquia del disseny 
set_db auto_ungroup none

#La primera optimització és la genèrica. Elimina redundàncies i unloaded logic i optimitza el datapath (incloent transformacions de carry save arithmetic (CSA))
syn_generic

#Tornem a fer un track del progrés
write_snapshot -outdir snapshot -tag generic
report_summary -directory snapshot

#Ara ja podem fer un mapping i una optimització de la tecnologia
#Sequential constant optimisation. Elimina les jerarquies no carregades.Logic mapping on a block by block basis
syn_map

#Optimització bloc per bloc amb recuperació i reestructuració d'àrea
syn_opt

#Tornem a fer un track del progrés
write_snapshot -outdir snapshot -tag mapped
report_summary -directory snapshot

#Després de l'optimització, es revisa que el disseny compilat encaixa amb les contraints
report_qor
report_summary

#Podem debugar el temps. Ens hem de fixar en l'slack. Si és positiu vol dir que el pitjor dels camins compleix les constraints.
report_timing

#Si en tenim molts, podem seleccionar només els pitjors n camins
report_timing -endpoints -max_paths 4

#Podem mirar l'àrea que ocupa el disseny
report_area -summary 
#Amb l'opció depth podem triar la profunditat de la jerarquia que volem veure. Per exemple per comparar l'àrea ocupada pel nucli i l'ocupada pels pads. S'ha de vigilar amb les comparacions relatives perquè ara no s'estan tenint en compte: ús del nucli, àrea pel routing de potència, floorplaning constraints, IO fillers, corner cells o bond pads.
report_area -depth 3 

#Ara mirem els recursos usats al disseny
#Ús de datapath logic
report_dp -all

#Mirem els latches i flip-flops
report_sequential -hier

#Podem mirar tota la jerarquia del disseny
report_hierarchy

#Podem mirar les diferents cel·les i els detalls e les seves llibreries
report_gates

#Podem mirar si hi ha timing loops. El static timing analysis els ha d'haver trencat prèviament.
report_loop > loop.rpt

#Amb la comanda ungroup podem desmuntar un nivell de jerarquia concreta.
#Si les constraints no ens serveixen podem modificar-les. Després les podem comparar amb
#exec diff -u fitxerconstraints1.tcl fitxerconstraints2.tcl

#Per carregar les noves constraints fem un reset i carreguem les noves:
#reset_design -timing
#read_sdc fitxerconstraints2.tcl

#I tornem a sintetitzar per arreglar les violacions temporals
#No em queda clar si abans s'han de fer les syn_generic i syn_map
#syn_opt

#I fem un track de la nova versió
#write_snapshot -outdir snapshot -tag mapped-incr
#report_summary -directory snapshot

#I tornem a mirar si es compleixen les constraints i com ha quedat l'area
#report_timing
#report_area -depth 3

#####
#GUARDAR EL DISSENY
#####

#Abans hem de canviar els noms de la netlist per estiguin en el llenguatge que ens interessa. Els passarem a verilog. Els warnings poden indicar que hi ha noms que no es canviaran sense l'opció -force, però no cal
change_names -verilog


#Guardem el binary database. És una fot completa del disseny tal i com està a la memòria de Genus. Així sempre podrem restaurar el disseny tal i com està ara.
write_db -to_file design.db
write_design -gzip -basename design/mapped

#Guardem la netlist de verilog per passar-ho al domini de Place and route
write_hdl > design.v

#També pot ser útil guardar les constraints enun fitxer "flat", tot i que en general és millor utilitzar les originals
if {!$is_mmmc} {write_sdc > design.sdc}
if {$is_mmmc} {
	foreach mode {typical fast slow} {write_sdc -view *$mode > sdc/functional_wcl_$mode.sdc}
	source scripts/genus_update_mmmc_constraints.tcl
}


#Podem fer una revisió ràpida per veure si la netlist és funcionalment equivalent al RTL utilitzant el Conformal LEC
file mkdir lec
write_hdl > lec/final.v
write_do_lec -revised lec/final.v -log rtl2g.log > lec/rtl2g.do
#Com que la sortida està limitada executem el Conformal LEC en una altra finestra de la terminal. Millor fer una finestra nova per no perdre el que tinguem corrent a altres terminals. Hem d'entrar a la carpeta del projecte del disseny.
#exec lec -LPGXL -nogui -do lec/rtl2g.do

####
#Recuperar un fitxer de verilog
####
#En algun momnet potser volem tornar enrere al disseny, examinar-lo o reoptimitzar-lo. Podem eliminar el disseny de la memòria i tornar-lo a carregar des del fitxer verilog:
#delete_obj design:.designs/*
#read_hdl design.v
#elaborate wrap_system_top

#Si ara revisem les timing constraints veurem que està unconstrained, ja que tot això no es guarda en el verilog
#report_timing -lint
#report_timing

#Hem de tornar a carregar les constraints amb el fitxer sdc.
#read_sdc design.sdc

#Si ara ho tornem a revisar veurem que ja hi ha constraints
#report_timing -lint
#report_timing

###

#També podem carregar el disseny des del fitxer binari:
#delete_obj design:.designs/*
#read:db design.db

#Mirem que estigui complet
check_design -unresolved

#I tornem a mirar les constraints. Aquestes sí que estan al fitxer binari. El fitxer db també conté gran part d'altres atributs com clock gating, test i atributs del datapath, per tant és millor per guardar una foto completa del disseny.
#report_timing -lint
#report_timing
#wire-load interconneting models. per veure quins models usa el (en les proves spi_slave) design quines característiques tenen
get_db design:$top_module .wireload
#vls -attr [get_db design:spi_slave .wireload] NO FUNCIONA!!!
#genus genera una path group per clock que tengui el disseny, i un de base per al disseny (que és el grup per defecte)
#vls cost_group:spi_slave/*
#report_qor

#també podem crear els nostres propis costs_groups, o paths groups. en aquest cas, per exemple fent grups in2out in2reg reg2out etc. (es configura al tcl que es crida)

source scripts/genus_RAL_create_path_groups.tcl


write_db -all_root_attributes -to_file save/post_map_opt.db
