###################################################
# Define basic physical constraints
###################################################

# Define the number of routing layers to use:
set_db number_of_routing_layers 6 

# Specify the design aspect ratio: 
get_db design:$top_module .aspect_ratio 1
set_db design:$top_module .aspect_ratio 1

report_ple

# Definim el floorplan en microns
create_floorplan $top_module -die_box "0 0 $w_die $h_die"
create_floorplan $top_module -die_box "0 0 $w_die $h_die" -core_box "$c_margin $c_margin [expr $w_die-$c_margin] [expr $w_die-$c_margin]" 
check_floorplan

#Escrivim el floorplan el un fitxer.def
write_def > design.def

#I el llegim
read_def design.def

#Fem una snapshot
write_snapshot -outdir snapshot -tag 0_import
#summary_table -outdir snapshot
report_summary -dir snapshot
#timestat generic


syn_generic
syn_map
syn_opt

#Fem una snapshot
write_snapshot -outdir snapshot -tag 1_import
#summary_table -outdir snapshot
report_summary -dir snapshot


