#Fitxer creat pel Joan


# MANUALLY TRANSLATE (WARN-12): Argument '-enableMultipleDriveNet' for command 'setAnalysisMode' is obsolete in common UI and is removed. 
# MANUALLY TRANSLATE (WARN-12): Argument '-warn' for command 'setAnalysisMode' is obsolete in common UI and is removed. 
# MANUALLY TRANSLATE (WARN-12): Argument '-skew' for command 'setAnalysisMode' is obsolete in common UI and is removed. 
# MANUALLY TRANSLATE (WARN-12): Argument '-log' for command 'setAnalysisMode' is obsolete in common UI and is removed. 
set_db timing_analysis_cppr both 
set_db timing_analysis_clock_gating 1
set_db timing_use_latch_time_borrow 1  
set_db delaycal_support_output_pin_cap 1 
set_db timing_case_analysis_for_sequential_propagation 1 
set_db timing_analysis_self_loops_paths_no_skew 0  
set_db timing_analysis_clock_source_paths 1 
set_db timing_analysis_useful_skew 1  
set_db timing_analysis_type ocv 
set_db timing_analysis_clock_propagation_mode sdc_control 

#comanda comentada a l'script original del curs, que estava en legacy
#set_global timing_defer_mmmc_object_updates true

#feim el necessarui per crear les analysis views, pel disseny, per poder fer el post-cts
# Delay corner = timing library plus rc corner
# Worst-case corner = max delay/affects setup times
# Best-case corner = min delay/affects hold times
# For 1-corner use typical values for both
#això ve definit a la mmmc file, que es fa amb genus. ara usam només UN cas, potser s'hauria de crear de nou usant wc i bc, com comenta el text anterior
#quan a la mmmc file fa el set_analysis_view, hauria de setejar una wc per al setup, i una bc per al hold
if {!$is_mmmc} {update_analysis_view -name default_emulate_view \
    -constraint_mode default_emulate_constraint_mode \
    -delay_corner default_emulate_delay_corner}

if {$is_mmmc} {
create_analysis_view -name view_wcl_slow \
    -constraint_mode functional_wcl_slow \
    -delay_corner delay_corner_wcl_slow

create_analysis_view -name view_wcl_fast \
    -constraint_mode functional_wcl_fast \
    -delay_corner delay_corner_wcl_fast

create_analysis_view -name view_wcl_typical \
    -constraint_mode functional_wcl_typical \
    -delay_corner delay_corner_wcl_typical
}

## setting up analysis_views
#L'ordre és important. La primera view serà la per defecte, que serà la usada per aquelles aplicacions que no suportin multi-mode
if {!$is_mmmc} {set_analysis_view -setup { default_emulate_view } \
                  -hold { default_emulate_view }}
if {$is_mmmc} {set_analysis_view -setup { view_wcl_slow view_wcl_fast view_wcl_typical } \
                  -hold { view_wcl_fast view_wcl_slow view_wcl_typical }}

if {!$is_mmmc} {set_interactive_constraint_modes {default_emulate_constraint_mode}
  set_propagated_clock [all_clocks]
  set_interactive_constraint_modes {}
}
if {$is_mmmc} {
  foreach mode {typical fast slow} {
    set_interactive_constraint_mode functional_wcl_$mode
    set_propagated_clock [all_clocks]
    set_interactive_constraint_modes {}
  }
}

