#Verification
#Cada una d'aquestes comandes genera un report (fitxer.rpt).



##########
##DRC
##########

get_multi_cpu_usage -local_cpu
gui_select -point {781.241 173.025}
get_multi_cpu_usage -local_cpu
get_multi_cpu_usage -cpu_per_remote_host
get_multi_cpu_usage -remote_host
get_multi_cpu_usage -keep_license
get_distributed_hosts -mode
set_multi_cpu_usage -local_cpu 4 -cpu_per_remote_host 1 -remote_host 0 -keep_license true
check_drc -out_file signoff_reports/design.drc.rpt



##########
##Connectivity
##########

check_connectivity -type all -error 1000 -warning 50 -out_file signoff_reports/design.conn.rpt
#Si hi ha més d'1 PAD d'alimentació no connecta bé i aquí tenim error

# Per veure els errors: Tools -> Violation Browser

# Podem  esborrar els errors
# delete_drc_markers


##########
##Antenna
##########

check_process_antenna -out_file signoff_reports/design.antenna.rpt

##########
##Power Via
##########

check_power_vias -report signoff_reports/design.power_vias.rpt
##Si hi ha més d'1 PAD d'alimentació no connecta bé i aquí tenim error

##########
##Power Analysis
##########

set_db power_method static
set_db power_corner max
set_db power_write_db true
set_db power_write_static_currents true
set_db power_honor_negative_energy true
set_db power_ignore_control_signals true
set_db power_grid_libraries ./pgv/techonly.cl



#Run
set_power_output_dir -reset
set_power_output_dir ./PA_results
set_default_switching_activity -reset
set_default_switching_activity -input_activity 0.2 -period 10.0
read_activity_file -reset
set_power -reset
set_dynamic_power_simulation -reset
set_pg_library_mode -cell_type techonly -extraction_tech_file $tech_path/qrc/qrcTechFile_typ -lef_layer_map $tech_path/qrc/lef2qrc.map

if {!$is_mmmc} {
set_db power_view default_emulate_view
report_power -rail_analysis_format VS -out_file ./PA_results/default_view_$top_module.rpt
}
if {$is_mmmc} {
  foreach mode {typical fast slow} {
    set_db power_view view_wcl_$mode
    report_power -rail_analysis_format VS -out_file ./PA_results/PA_${mode}_$top_module.rpt
  }
}


##########
##Rail Analysis
##########

#S'ha d'anar canviant el nom de la Power Grid Library cada vegada que s'executa el Early Rail Analysis

set_rail_analysis_mode -method static -power_switch_eco false -accuracy hd -power_grid_libraries ./pgv/techonly.cl -process_techgen_em_rules true -enable_rlrp_analysis false -extraction_tech_file $tech_path/qrc/qrcTechFile_typ -voltage_source_search_distance 50 -ignore_shorts false -enable_manufacturing_effects false -report_via_current_direction false

#Escrivim la ubicació dels pads de VCC
write_power_pads -net VCC -auto_fetch
gui_set_draw_view place
write_power_pads -net VCC -voltage_source_file ./RA_results/design.pp

####Run

set_pg_nets -net VCC -voltage 1.62 -threshold 1.458
set_power_data -reset
set_power_data -format current -scale 1 PA_results/static_VCC.ptiavg
set_power_pads -reset
set_power_pads -net VCC -format xy -file ./RA_results/design.pp
set_package -reset
set_package -spice_model_file {} -mapping_file {}
set_net_group -reset
set_advanced_rail_options -reset
report_rail -type net -output_dir ./RA_results VCC

#### Per veure els resultats: Power -> Report -> Power & Rail Result ...
### Clicar DB Setup i definir: Power Database i RA database (l'últim que s'hagi generat). Es pot fer amb la següent comanda canviant el número de la DB
#read_power_rail_results -power_db PA_Results/power_6.db -rail_directory RA_results/VCC_25C_avg_5 -instance_voltage_window { timing  whole  } -instance_voltage_method {  worst  best  avg  worstavg }

## A Layer/Nets posar al mínim l'opacitat del disseny i de la DB

#### Lab2.6 p11





##########
##DRC de Calibre
##########

#Fem un DRC com a la p17 Lab 2.6
