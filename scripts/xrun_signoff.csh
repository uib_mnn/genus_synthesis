#!/bin/csh

rm -r xcelium.d

#Compilam el sdf que tenim de sortida del PnR, quan acabam amb innovus. (crea un arxiu amb el mateix nom, pero .X)
xmsdfc OutputFiles/maxmin_top_final.sdf -output maxmin_top_final.sdf.X


#Cridam la comanda de xrun, que inclou el CMD, on en aquest arxiu posam quin arxiu .X ha de cridar. També s'hi ha d'indicar l'scope, que és la instància que volem simular, és a dir el device under test DUT. L'hem de cridar indicant qel mòdul top del testbench.
xrun -mcl 8 +access+r -sv OutputFiles/sim_netlist_maxmin_top.v tb/tb_maxmin_top_verilog_edited.v -vlog_ext +.lib.src -f scripts/filelist_signoff.f -sdf_cmd_file scripts/sdf.cmd -sdf_verbose +delay_mode_path -notimingchecks -input scripts/xrun.tcl

mv sim/testpattern.vcd sim/gate_netlist_sim.vcd

#S'utilitza l'arxiu filelist.f, que crida el disseny/netlist generat quan acabam amb innovus, també usa el test bench (tb) spi_slave_tb_gate.v. i carrega les llibreries (.lib.v) per simular a nivell de portes de la tecnologia.

#Falta implementar la part del Lab3.4 p.6, on torna a simular amb switchos addicionals (això seria canviar el testbench per una altra situació d'estimuls) 

#Seria bo provar de fer simulacions usant els valors de sdf, en un cas els valors mínims de delays, i en l'altre cas els valors màxims de delays.
