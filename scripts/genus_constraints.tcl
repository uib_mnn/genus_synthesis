#COM AFEGIR CONSTRAINTS al DESIGN
#primer BORRAR les que tengui (OJO també reseteja tots els altres atributs del disseny, i no s'ha d'usar en una netlist mappejada ja)
#reset_design

#Les unitats aquí estan en ns, però Genus les msotra en ps


#miram que el design està complert
check_design -unresolved

#una millor opció, és borrar només tot el referent a timing
#Compte! Si s'ha definit mmmc amb aixo ens ho carreguem tot.
reset_design -timing


#Crea un clock anomenat clk
#El period està en ns
set period [expr (1./$freq)*pow(10,9)]
create_clock [get_ports $in_clk] -name clk -period $period

#Definex la latency en ns
set_clock_latency 0.8 clk

#Defineix la uncertainty = skew + jitter en ns
#Definim el setup i el clock en funció del període.
set_clock_uncertainty -setup [expr 0.1*$period] clk
set_clock_uncertainty -hold [expr 0.05*$period] clk

#Defineix la transition en ns
set_clock_transition 0.1 clk

#Mostra els clocks
#report_clocks

#Si volem evitar que l'eina de síntesi modifiqui una net eg. el reset per reduir temps de processament. En aquest cas tenim el chip select del SPI que és la net i_ss:
if {![$in_en == ""]} {set_ideal_network [get_ports $in_en]}
if {![$in_rst == ""]} {set_ideal_network [get_ports $in_rst]}

#Defineix el rang de l'output delay. Evita que la clock network latency s'apliqui al càlcul de l'output. Valors en ns.
set_output_delay 2.0 -max -network_latency_included -clock clk [all_outputs] 
set_output_delay 0.1 -min -network_latency_included -clock clk [all_outputs] 

#Defineix el rang de la càrrega en pf
set_load 1.0 -max [all_outputs]
set_load 0.01 -min [all_outputs]

#Defineix el rang de l'input delay. Evita que la clock network latency s'apliqui al càlcul de l'input. En ns
set_input_delay 2.0 -max -network_latency_included -clock clk [all_inputs]
set_input_delay 0.1 -min -network_latency_included -clock clk [all_inputs] 

#Defineix el rang de la external driving resistance en kohms
set_drive 0.4 -max [all_inputs]
set_drive 0.01 -min [all_inputs]

#De totes maneres, és millor si tenim la cel·la d'una driving resistance.

#Slow
set_driving_cell -max -lib_cell BUF8 -library fsa0a_c_generic_core_ss1p62v125c -pin O [all_inputs]
set_driving_cell -min -lib_cell BUF1 -library fsa0a_c_generic_core_ss1p62v125c -pin O [all_inputs]
write_sdc > sdc/functional_wcl_slow.sdc


#Revisa que totes les constraints estiguin posades
report_timing -lint
report_port -driver *
report_port -load *
report_port -delay *

#Es poden escriure en format legacy Genus:
#write_script > design.genus_constr.tcl
#exec nedit design.genus_const.tcl &

#Check timing constraints
validate_constraints -rtl -detail

###Agrupa els paths
#group_path -name in2reg  -from [all_inputs]
#group_path -name reg2out -to   [all_outputs]
#group_path -name in2out  -from [all_inputs]  -to [all_outputs]
#group_path -name reg2reg -from [all_registers]  -to [all_registers]

syn_generic
syn_map 
syn_opt


write_db -all_root_attributes -to_file save/constrained.db
