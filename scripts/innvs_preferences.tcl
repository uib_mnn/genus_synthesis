set_preference ConstraintUserXOffset 0.01
set_preference ConstraintUserYGrid 0.01
set_preference ConstraintUserXGrid 0.01
set_preference ConstraintUserYOffset 0.01

set_preference SnapAllCorners 1

floorplan_set_snap_rule -for CON -grid UG -force
floorplan_set_snap_rule -for BLK -grid UG -force
floorplan_set_snap_rule -for PLK -grid UG -force
floorplan_set_snap_rule -for IOP -grid UG -force
floorplan_set_snap_rule -for DIE -grid UG -force
floorplan_set_snap_rule -for CORE -grid UG -force

