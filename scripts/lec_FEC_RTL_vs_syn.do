//Fitxer per executar a la terminal de lec
//El programa s'engega amb lec -lpgxl
//Comanda: dofile scripts/lec_FEC_RTL_vs_syn.do

//Podem executar-ho tot de cop amb lec -d scripts/lec_FEC_RTL_vs_syn.do -lpgxl

//Per fer un reset, utilitzar la comanda: reset




//Configuració inicial
set datapath option -share -auto -module -verbose
set analyze option -auto

//Afegim les llibreries
//add search path -library /eda/pdk/UMC/18/TECH/lib /eda/pdk/UMC/18/_G-01-LOGIC18-1.8V-3.3V-1P6M-GENERICII_Faraday-IP/Design_Package/foa0a_o/2012Q4v2.1/T33_GENERIC_CD_IO/FrontEnd/synopsys/synthesis

add search path -library /home/arnau/docs/Tecnologia/lib

read library -statetable -liberty  -both  \
	fsa0a_c_generic_core_ss1p62v125c.lib \
	fsa0a_c_generic_core_ff1p98vm40c.lib \
	fsa0a_c_generic_core_tt1p8v25c.lib \
	foa0a_o_t33_generic_cd_io_ss1p62v125c.lib \
	foa0a_o_t33_generic_cd_io_ff1p98vm40c.lib \
	foa0a_o_t33_generic_cd_io_tt1p8v25c.lib

//Llegim el RTL al contenidor Golden
//add search path -design ../common/rtl/
//L'opcio -rootonly fa que sols s'elabori el top i els moduls instanciats. Aixo evita que es generin falsos top modules.
read design -golden -lastmod -noelab -vhdl 93 src/maxmin_top.vhd -root maxmin_top -map work src -rootonly

//set rule handling RTL7.16 -warning
elaborate design -golden -root maxmin_top


//Llegim la netlist sintetitzada sense PADs al contenidor Revised
read design -verilog -revised -lastmod -noelab syn_results/design.save/design.v
elaborate design -revised -root maxmin_top

//Comparem els dissenys
report design data
report black box

//Com que durant la síntesi hem afegit elements lògics, desabilitem scan chain perquè no hi hagi problemes d'equivalència entre el RTL i la Netlist
//No ho posem perquè el nostre disseny no té scan chain
//add pin constraints 0 se -both

//Igualment, surant la síntesi s'han modificat les cel·les seqüencials (afegit, eliminat, unit,...). Configurem el programa perquè ho tingui en compte
set flatten model -seq_constant
set flatten model -all_seq_merge
set flatten model -seq_redundant
set flatten model -seq_constant_x_to 0
set flatten model -gated_clock

//Cada instància del mòdul es pot implementar usant diferents cel·les segons les constraints. Per això el mòdul sintetitzat pot canviar mentre el RTL es manté igual. Amb la següent comanda es fan còpies de cada instància per poder fer modificacions independentment.
uniquify -all -nolib -golden -verbose

//Per fer la comparació hem de canviar el mode de setup a LEC
//Fem un flattening, remodelling, mapping i improve datapath matching
set system mode lec

//Podem revisar els resultats del mapeig
report mapped points -summary
report unmapped points -summary
report unmapped points -unreach -nodlat_gated_clock -revised

//Fem la comparació
add compare point -all
compare


//Fem un report de l'estat del disseny
report statistics
report verification
report verification -compare_result

