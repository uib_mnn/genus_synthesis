#Afegim vias entre les capes 4 i 5 per connectar el PAD amb el bondpad (requeriment de les llibreries que usem).
#Després de crear cada via li assignem la net del port perintent perquè no doni un fals curtcircuit.
set pad_cells "$in_cell $out_cell $power_cells_1_8 $power_cells_3_3"
set pad_insts ""
foreach inst "[get_db insts .name]" {if {"[get_db inst:$inst .base_cell.base_name]" in $pad_cells} {append pad_insts "$inst "}}


set via_width 40
set via_height 2
set cut_side 0.28
set cut_spacing 0.3
set via_enclosure 0.5

foreach inst $pad_insts {
  switch [get_db inst:$inst .orient] {
    "r0" {
	set xpos [expr [get_db inst:$inst .location.x] + $pad_width/2]
	set ypos [expr [get_db inst:$inst .location.y] + $via_height/2 + $via_enclosure]
	set columns [expr int($via_width/($cut_side+$cut_spacing))]
	set rows [expr int($via_height/($cut_side+$cut_spacing))]
    }
    "r90" {
	set xpos [expr [get_db inst:$inst .location.x] + $pad_height - $via_height/2 - $via_enclosure]
	set ypos [expr [get_db inst:$inst .location.y] + $pad_width/2]
	set rows [expr int($via_width/($cut_side+$cut_spacing))]
	set columns [expr int($via_height/($cut_side+$cut_spacing))]
    }
    "r180" {
	set xpos [expr [get_db inst:$inst .location.x] + $pad_width/2]
	set ypos [expr [get_db inst:$inst .location.y] + $pad_height - $via_height/2 - $via_enclosure]
	set columns [expr int($via_width/($cut_side+$cut_spacing))]
	set rows [expr int($via_height/($cut_side+$cut_spacing))]
    }
    "r270" {
	set xpos [expr [get_db inst:$inst .location.x] + $via_height/2 + $via_enclosure]
	set ypos [expr [get_db inst:$inst .location.y] + $pad_width/2]
	set rows [expr int($via_width/($cut_side+$cut_spacing))]
	set columns [expr int($via_height/($cut_side+$cut_spacing))]
    }
  }
  edit_set_via -auto_update 1 -auto_snap 1 -columns $columns -create_by geometry -cut_layer VI4 -rows $rows -x_bottom_enclosure $via_enclosure -y_bottom_enclosure $via_enclosure -x_top_enclosure $via_enclosure -y_top_enclosure $via_enclosure -x_size $cut_side -y_size $cut_side -x_space $cut_spacing -y_space $cut_spacing
  edit_create_via "$xpos $ypos"
  delete_markers
  select_vias -nets _NULL
  set net_port ""
  if {"[get_db [get_db insts $inst] .base_cell.base_name]" in "$power_cells_1_8 $power_cells_3_3"} {set net_port [get_db [get_db insts $inst] .base_cell.pg_base_pins]} else {foreach net_port [get_db [get_db insts $inst] .pins.net] {if {[regexp "_pt" $net_port]} {break}}}
  edit_update_route_net -to [get_db $net_port .base_name]
}

