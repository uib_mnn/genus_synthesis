
##################
#Paràmetres
##################


###### La carpeta de cada projecte ha d'incloure: ######
#1. Una carpeta src amb tots els .vhd (top i altres)
#2. Una carpeta tb amb el testbench
#3. Una carpeta (o enllaç) scripts amb els scripts.
#3. Un fitxer setup_vars.tcl on s'hi han de definir les següents variables:

###### Paràmetres comuns Genus/Innovus: #######
#top_module: top module name
#w_die: Ample del die
#h_die: Alçada del die
#c_margin: Marge entre el core i el die
#in_clk: clock port (in RTL)
#in_en: enable port (in RTL)
#in_rst: reset port (in RTL)
#is_mmmc: true o false, indica si fem un Mult-Mode Multi-Corner Analysis
#freq: Clock frequency (Hz)
#simtime: El temps de simulacio ha de ser una var d'entorn per passar-la a xcelium. En ns.

###### Paràmetres exclusius de Genus: ######
#vhdl_src_file: RTL source file
#vhd_libs: libraries for the vhdl module. Han d'estar en ordre segons les dependències internes.
#vhdl_lib_path: on estan les llibreries vhdl
#tb_file: testbench

#
set_db source_verbose true 

source setup_vars.tcl

set freqMHz "[expr int($freq/pow(10,6))]MHz"
set_db log_file genus$freqMHz.log


##################
#Flow
##################
#Guardem el moment d'inici del flow
set flow_start_time [clock seconds]


#Executem el run nomes al localhost
set_db super_thread_servers {}
#Posem els límits de cpus (el servidor en té 56). Genus utilitza una llicncia per cada 8 cpus.
#set_db max_cpus_per_server 8
set_db max_cpus_per_server 16
# Lmit del directori cache (document d'emmagatzemamatge intermig). Augmentar aix dona problemes.
#set_db max_super_thread_cache_size 2000
#set_db super_thread_debug_directory super_thread_debug_directory


#El disseny està guardat en difernts punts del flow dins la carpeta save/
#El podem recuperar en aquell punt amb la comanda read_db


#Hem d'esborrar la capreta xcelium.d perquè guarda info d'altres simulacions (llibreries) i pot donar error si no simulem el mateix (entre RTL i gate-level, per exemple).
if {[file exists xcelium.d]} {rm -r xcelium.d}
if {[file exists sim] == 0} {file mkdir sim}
exec xrun -mcl 8 +access+r -sv -v93 -messages $vhdl_src_file $tb_file -input scripts/xrun.tcl -makelib work $vhd_libs_string -endlib 
exec mv sim/testpattern.vcd sim/sim_rtl.vcd


if {[get_db lef_library] == ""} {source scripts/genus_setup.tcl}
source scripts/genus_load_design.tcl
if {!$is_mmmc} {source scripts/genus_constraints.tcl}
if {$is_mmmc} {source scripts/genus_set_mmmc_constraints.tcl}
source scripts/genus_map_opt.tcl
source scripts/genus_power.tcl
source scripts/genus_syn_phys.tcl

report_qor

puts "\n\n\t\t\t#########################################\n\n\t\t\tFinal del flow\n\n\t\t\t#########################################\n\n\n"

#Guardem el moment final del flow i mostrem la durada
set flow_end_time [clock seconds]

puts "El contingut del fitxer setup_vars.tcl es:\n"
puts [read [open setup_vars.tcl r]]

puts "\n\nEl flow ha començat [clock format $flow_start_time -format {el %D a les %H:%M:%S}] i ha acabat [clock format $flow_end_time -format {el %D a les %H:%M:%S}].\n\n"

exit



