###### Simulacions RTL i Post-P&R ######
  #Creem primer el testbench amb el nom dels nous ports.
  regsub {edited} $tb_file "pnr_edited" tb_file_post_pnr
  set tbfile [open $tb_file r]
  set fdata [read $tbfile]
  close $tbfile

  if {[file exists $tb_file_post_pnr]} {rm $tb_file_post_pnr} 
  set tbfile_edit [open $tb_file_post_pnr w]
  foreach p "$in_pinbus_list $out_pinbus_list" {
    regsub "$p " $fdata "${p}_pt " fdata
  }
  puts $tbfile_edit $fdata
  close $tbfile_edit

#Simulem per a les diferents imatges d'entrada
#for {set i 0} {$i < $tb_img_num} {incr i} {
  if {[file exists xcelium.d]} {rm -r xcelium.d}

  exec xrun -mcl 8 +access+r -sv -v93 -messages $vhdl_src_file tb/tb_maxmin_top_verilog_edited.v -input scripts/xrun.tcl -makelib work $vhd_libs_string -endlib
  mv sim/testpattern.vcd sim/sim_rtl_img$i.vcd
  

  rm -r xcelium.d

  #Compilam el sdf que tenim de sortida del PnR, quan acabam amb innovus. (crea un arxiu amb el mateix nom, pero .X)
  exec xmsdfc OutputFiles/maxmin_top.sdf -output maxmin_top.sdf.X

#Cridam la comanda de xrun, que inclou el CMD, on en aquest arxiu posam quin arxiu .X ha de cridar. També s'hi ha d'indicar l'scope, que és la instància que volem simular, és a dir el device under test DUT. L'hem de cridar indicant qel mòdul top del testbench.

  exec xrun -mcl 8 +access+r -sv OutputFiles/sim_netlist_maxmin_top.v $tb_file_post_pnr -vlog_ext +.lib.src -v $tech_path/verilog/foa0a_o_t33_generic_cd_io_30.lib.src -v $tech_path/verilog/fsa0a_c_generic_core_30.lib.src -timescale 1ns/1ps -sdf_cmd_file scripts/sdf.cmd -sdf_verbose +delay_mode_path -notimingchecks -input scripts/xrun.tcl

  exec mv sim/testpattern.vcd sim/gate_netlist_sim_img$i.vcd
#}
