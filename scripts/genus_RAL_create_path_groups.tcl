
if {!$is_mmmc} {
  if {[llength [all_registers]] > 0} { 
    define_cost_group -name in2reg -weight 1 -design /designs/*
    path_group -from [all_inputs]  -to [all_registers] -group in2reg -name in2reg
    define_cost_group -name reg2out -weight 1 -design /designs/*
    path_group -from [all_registers] -to [all_outputs] -group reg2out -name reg2out
    define_cost_group -name reg2reg -weight 2 -design /designs/*
    path_group -from [all_registers] -to [all_registers] -group reg2reg -name reg2reg
  }
  define_cost_group -name in2out -weight 1 -design /designs/*
  path_group -from [all_inputs]  -to [all_outputs] -group in2out -name in2out
  #path_group -from /designs/*/ports_in/* -to /designs/*/ports_out/* -group in2out -name in2out
}


if {$is_mmmc} {
foreach mode {typical fast slow} {
  set_interactive_constraint_mode *$mode
  if {[llength [all_registers]] > 0} { 
    define_cost_group -name in2reg -weight 1 -design /designs/*
 puts $mode
    path_group -from [all_inputs]  -to [all_registers] -group in2reg -name in2reg -view *$mode
    define_cost_group -name reg2out -weight 1 -design /designs/*
    path_group -from [all_registers] -to [all_outputs] -group reg2out -name reg2out -view *$mode
    define_cost_group -name reg2reg -weight 2 -design /designs/*
    path_group -from [all_registers] -to [all_registers] -group reg2reg -name reg2reg -view *$mode
  }
  define_cost_group -name in2out -weight 1 -design /designs/*
  path_group -from [all_inputs]  -to [all_outputs] -group in2out -name in2out -view *$mode
  #path_group -from /designs/*/ports_in/* -to /designs/*/ports_out/* -group in2out -name in2out
  set_interactive_constraint_mode ""
}
}
