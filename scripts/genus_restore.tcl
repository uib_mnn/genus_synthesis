reset_design
#source scripts/setup.tcl
#source scripts/setup_phys.tcl

#Carreguem el disseny des del fitxer binari:
#delete_obj design:.designs/*
read_db design.db

#Mirem que estigui complet
check_design -unresolved

#I tornem a mirar les constraints. Aquestes sí que estan al fitxer binari. El fitxer db també conté gran part d'altres atributs com clock gating, test i atributs del datapath, per tant és millor per guardar una foto completa del disseny.
report_timing -lint
report_timing

