#Actualitzem les constraints per fer un anàlisi de temps post cts

#TRANSLATE (WARN-12): Argument '-enableMultipleDriveNet' for command 'setAnalysisMode' is obsolete in common UI and is removed. 
# MANUALLY TRANSLATE (WARN-12): Argument '-warn' for command 'setAnalysisMode' is obsolete in common UI and is removed. 
# MANUALLY TRANSLATE (WARN-12): Argument '-skew' for command 'setAnalysisMode' is obsolete in common UI and is removed. 
# MANUALLY TRANSLATE (WARN-12): Argument '-log' for command 'setAnalysisMode' is obsolete in common UI and is removed. 


set_db timing_analysis_cppr both
set_db timing_analysis_clock_gating 1
set_db timing_use_latch_time_borrow 1
set_db delaycal_support_output_pin_cap 1
set_db timing_case_analysis_for_sequential_propagation 1
set_db timing_analysis_self_loops_paths_no_skew 0
set_db timing_analysis_clock_source_paths 1
set_db timing_analysis_useful_skew 1
set_db timing_analysis_type ocv
set_db timing_analysis_clock_propagation_mode sdc_control 

#set_global timing_defer_mmmc_object_updates true

update_analysis_view -name func_view_wc -constraint_mode func_mode_prop
update_analysis_view -name func_view_bc -constraint_mode func_mode_prop
update_analysis_view -name test_view_wc -constraint_mode test_mode_prop
update_analysis_view -name test_view_bc -constraint_mode test_mode_prop