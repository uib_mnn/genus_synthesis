
## setting constraint_modes

update_constraint_mode -name functional_wcl_slow \
    -sdc_files { ./sdc/functional_wcl_slow.sdc }

update_constraint_mode -name functional_wcl_fast \
    -sdc_files { ./sdc/functional_wcl_fast.sdc }

update_constraint_mode -name functional_wcl_typical \
    -sdc_files { ./sdc/functional_wcl_typical.sdc }

##creating analysis_views

update_analysis_view -name view_wcl_slow \
    -constraint_mode functional_wcl_slow \
    -delay_corner delay_corner_wcl_slow

update_analysis_view -name view_wcl_fast \
    -constraint_mode functional_wcl_fast \
    -delay_corner delay_corner_wcl_fast

update_analysis_view -name view_wcl_typical \
    -constraint_mode functional_wcl_typical \
    -delay_corner delay_corner_wcl_typical

## setting up analysis_views
#L'ordre és important. La primera view serà la per defecte, que serà la usada per aquelles aplicacions que no suportin multi-mode
set_analysis_view -setup { view_wcl_slow view_wcl_fast view_wcl_typical } \
                  -hold { view_wcl_fast view_wcl_slow view_wcl_typical }
