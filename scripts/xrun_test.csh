#!/bin/csh

##### Simulacions RTL i Gate-level ######

###Aquest script necessita que hi hagi un fitxer OutputFiles/xrun_vars.csh#####

set period = 100
set in_img_path = "input_image/x_test_img_test500.csv"
set out_rtl_csv = "output_csv/x_test_img_output_test500_rtl.csv"
set out_pnr_csv = "output_csv/x_test_img_output_test500_pnr.csv"
set rtl_exp_csv = "output_csv/x_test_img_output_expected.csv"
set pnr_exp_csv = "output_csv/x_test_img_output_expected.csv"

if ( -d xcelium.d ) then
	rm -r xcelium.d
endif

#Agafem les variables del fitxer setup_vars.tcl
source OutputFiles/xrun_vars.csh

set tb_file = "tb/tb_maxmin_top_verilog_test500_mask.v"
set tb_file_pnr = "tb/tb_maxmin_top_verilog_test500_pnr_mask.v"

xrun -mcl 8 -nowarn DLCPTH +access+r -v93 -messages $vhdl_src_file $tb_file -input scripts/xrun_no_vcd.tcl -makelib work $vhd_libs_string -endlib

#Guardem el log i la imatge de sortida
mv xrun.log xrun_rtl.log
mv output_csv/x_test_img_output.csv $out_rtl_csv

#Fem un nou testbench amb els noms nous dels ports
set lines = `grep '[[:blank:]]\.' tb/tb_maxmin_top_verilog_test500.v`

cp $tb_file $tb_file_pnr 
foreach port ($lines)
	set var = `echo $port | grep '\.'`
	if ( $var != "" ) then
		set var = \\$var
		sed -i "s/$var/${var}_pt/" $tb_file_pnr 
	endif
end


rm -r xcelium.d

#Compilam el sdf que tenim de sortida del PnR, quan acabam amb innovus. (crea un arxiu amb el mateix nom, pero .X)
xmsdfc OutputFiles/maxmin_top.sdf -output maxmin_top.sdf.X

#Cridam la comanda de xrun, que inclou el CMD, on en aquest arxiu posam quin arxiu .X ha de cridar. També s'hi ha d'indicar l'scope, que és la instància que volem simular, és a dir el device under test DUT. L'hem de cridar indicant qel mòdul top del testbench.

xrun -mcl 8 -nowarn DLCPTH +access+r -sv OutputFiles/sim_netlist_maxmin_top.v $tb_file_pnr -vlog_ext +.lib.src -f scripts/filelist_signoff.f -sdf_cmd_file scripts/sdf.cmd -sdf_verbose +delay_mode_path -notimingchecks -input scripts/xrun_no_vcd.tcl


mv xrun.log xrun_pnr.log
mv output_csv/x_test_img_output.csv $out_pnr_csv


echo "\n\n\n************************\n COMPARING OUTPUT FILES WITH comm -3 \n************************\n\n\n"

#set out_rtl_csv = `echo $out_rtl_csv | sed 's/\\//'`
#set out_pnr_csv = `echo $out_pnr_csv | sed 's/\\//'`

#La comanda comm -3 compara els dos fitxers i en retorna la diferència.
set compare = `comm -3 --nocheck-order $out_rtl_csv $rtl_exp_csv`
if ( "$compare" == "" ) then
	echo "PASS: El CSV de la simulació RTL és l'esperat\n"
else 
	echo "FAIL: El CSV de la simulació RTL no és correcte\n"
endif

set compare = `comm -3 --nocheck-order $out_pnr_csv $pnr_exp_csv`
if ( "$compare" == "" ) then
	echo "PASS: El CSV de la simulació post-P&R és l'esperat\n"
else 
	echo "FAIL: El CSV de la simulació post-P&R no és correcte\n"
endif







