//Fitxer per executar a la terminal de lec
//El programa s'engega amb lec -lpgxl
//Comanda: dofile scripts/lec_FEC_syn_vs_PnR.do

//Podem executar-ho tot de cop amb lec -d scripts/lec_FEC_syn_vs_PnR.do -lpgxl

//Per fer un reset, utilitzar la comanda: reset

//Configuració inicial
set datapath option -share -auto -module -verbose
set analyze option -auto

//Afegim les llibreries
//add search path -library /eda/pdk/UMC/18/TECH/lib /eda/pdk/UMC/18/_G-01-LOGIC18-1.8V-3.3V-1P6M-GENERICII_Faraday-IP/Design_Package/foa0a_o/2012Q4v2.1/T33_GENERIC_CD_IO/FrontEnd/synopsys/synthesis /eda/pdk/UMC/18/_G-01-LOGIC18-1.8V-3.3V-1P6M-GENERICII_Faraday-IP/Design_Package/fsa0a_c/2009Q2v2.0/GENERIC_CORE/BackEnd/lef/

add search path -library /home/arnau/docs/Tecnologia/lib home/arnau/docs/Tecnologia/lef

read library -statetable -liberty  -both  \
	fsa0a_c_generic_core_ss1p62v125c.lib \
	fsa0a_c_generic_core_ff1p98vm40c.lib \
	fsa0a_c_generic_core_tt1p8v25c.lib \
	foa0a_o_t33_generic_cd_io_ss1p62v125c.lib \
	foa0a_o_t33_generic_cd_io_ff1p98vm40c.lib \
	foa0a_o_t33_generic_cd_io_tt1p8v25c.lib 



//Llegim la netlist amb els PADs al contenidor Golden
//add search path -design ../common/rtl/
read design  -verilog -golden -lastmod -noelab syn_results/design.save/design_edited.v
elaborate design -golden -root maxmin_top

//Llegim la netlist sintetitzada al contenidor Revised
read design -verilog -revised -lastmod -noelab OutputFiles/lec_netlist_maxmin_top.v
elaborate design -revised -root maxmin_top

//Comparem els dissenys
report design data
report black box

//Com que durant la síntesi hem afegit elements lògics, desabilitem scan chain perquè no hi hagi problemes d'equivalència
//No ho posem perquè el nostre disseny no té scan chain
//add pin constraints 0 se -both

//Design constraints per a que pugui comparar el netlist d'entrada a innovus, i el de sortida d'innovus. en aquest cas al disseny gold li falten els TIE HI o LO, segons pertoqui, i les té flotants
//hi ha el manual de commandes a /eda/cadence/2017-18/RHELx86/CONFRML_17.10.240/doc/Conformal_Ref.pdf

//S'ha de revisar quins pins estan en pullup i en pulldown
add tied signals 1 *_inst/PD *_inst/SMT *_inst/E *_inst/E2 *_inst/E4 *_inst/E8 -pin -golden
add tied signals 0 *_inst/PU *_inst/SR -pin -golden



//Igualment, durant el P&R s'ha modificat el disseny. Configurem el programa perquè ho tingui en compte
set flatten model -seq_constant
set flatten model -all_seq_merge
set flatten model -seq_redundant
set flatten model -seq_constant_x_to 0
set flatten model -gated_clock

//Cada instància del mòdul es pot implementar usant diferents cel·les segons les constraints. Per això el mòdul sintetitzat pot canviar mentre el RTL es manté igual. Amb la següent comanda es fan còpies de cada instància per poder fer modificacions independentment.
uniquify -all -nolib -golden -verbose

//Per fer la comparació hem de canviar el mode de setup a LEC
//Fem un flattening, remodelling, mapping i improve datapath matching
set system mode lec

//Podem revisar els resultats del mapeig
report mapped points -summary
report unmapped points -summary
report unmapped points -unreach -nodlat_gated_clock -revised

//Fem la comparació
add compare point -all
compare


//Fem un report de l'estat del disseny
report statistics
report verification
report verification -compare_result


