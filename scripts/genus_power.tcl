

write_design -basename design/post_map_opt
if {[file exists xcelim.d]} {rm -r xcelium.d}

exec xrun -mcl 8 +access+r -sv -messages -input scripts/xrun.tcl -timescale 1ns/1ps design/post_map_opt.v $tb_file -v $tech_path/verilog/fsa0a_c_generic_core_30.lib.src

mv sim/testpattern.vcd sim/sim_post_map_opt.vcd
read_vcd -static sim/sim_post_map_opt.vcd
#Creem i llegim el .saif
write_saif > design.saif
read_saif design.saif


#És possible llegir diferents SAIFs de diferents testbenchs i donar-los pesos diferents
# Merge saif with weights of 50, 30, 20 (for tb1, tb2, and tb3 respectively)
#read_saif -instance nom_instancia fitxerTB1.saif
#read_saif -instance nom_instancia fitxerTB2.saif -update -weight [expr 30.0/50.0]
#read_saif -instance nom_instancia fitxerTB3.saif -update -weight [expr 20.0/(50.0+30.0)]

###################################################
# Enable power optimisation (Leakage and power Opt)
###################################################
#set_db designs .max_leakage_power 0
set_db designs .max_dynamic_power 0
#set_db leakage_power_effort medium
set_db leakage_power_effort medium

#Tria el pes que tenen els diferents consums en la optimització
# Use the following power multipliers:
#   99% leakage power
#   1% dynamic power
set_db designs .lp_power_optimization_weight 0.99

#Optimitza
syn_generic
syn_map

#Guardem el binary database. És una fot completa del disseny tal i com està a la memòria de Genus. Així sempre podrem restaurar el disseny tal i com està ara.
write_db -to_file design.db
write_design -gzip -basename design/mapped

#Guardem la netlist de verilog per passar-ho al domini de Place and route
write_hdl > design.v

#També pot ser útil guardar les constraints enun fitxer "flat", tot i que en general és millor utilitzar les originals
if {!$is_mmmc} {write_sdc > design.sdc}
if {$is_mmmc} {foreach mode {typical fast slow} {write_sdc -view *$mode > solutions/design_$mode.sdc}}

#Podem fer una revisió ràpida per veure si la netlist és funcionalment equivalent al RTL utilitzant el Conformal LEC
file mkdir lec
write_hdl > lec/final.v
write_do_lec -revised lec/final.v -log rtl2g.log > lec/rtl2g.do
#Com que la sortida està limitada executem el Conformal LEC en una altra finestra de la terminal. Millor fer una finestra nova per no perdre el que tinguem corrent a altres terminals. Hem d'entrar a la carpeta del projecte del disseny.
#exec lec -LPGXL -nogui -do lec/rtl2g.do


write_db -all_root_attributes -to_file save/power_optimized.db
