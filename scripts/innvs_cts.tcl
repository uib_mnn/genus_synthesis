


#Aquí en el curs d'Europractice actualitzen les constraints
#Active els corners


if {$is_mmmc} {
foreach mode {slow typical fast} {
update_constraint_mode -name [get_db constraint_modes .name *$mode] -sdc_files [list scripts/innvs_sdc.tcl]
}}

if {!$is_mmmc} {update_constraint_mode -name default_emulate_constraint_mode -sdc_files [list scripts/innvs_sdc.tcl]}

#set_timing_derate -data -delay_corner delay_wc -cell_delay -cell_check -early 0.95
#set_timing_derate -data -delay_corner delay_wc -net_delay -early 0.97

set_db route_design_with_litho_driven false

#Configurem el tipus d'anàlisi
set_db timing_analysis_type ocv
set_db timing_analysis_cppr both

#Definim cel·les pel CT
set_db cts_buffer_cells BUF*CK
set_db cts_inverter_cells INV*CK
#Per al clock gating triem les cel·les asíncrones.
set_db cts_clock_gating_cells GCKET*

#La següent comanda especifica un corner.
#Si no ho especifiquem el programa agafa el primer corner.
#Amb la comanda get_db delay corners només ens en retorna un: delay_corner:spi_slave/default_emulate_delay_corner
#Per tant, no cal especificar-lo
#set_db cts_primary_delay_corner delay_wc
#set_ccopt_property -delay_corner delay_bc -late target_skew auto
#set_ccopt_property -delay_corner delay_bc -early target_skew auto

#Les següents comandes també fan referència a una actualització de les constraints i els corners.
set_db cts_target_skew_late  auto
set_db cts_target_skew_early auto
set_db cts_target_skew 0.3
set_db cts_update_io_latency true

# NanoRoute settings for clock routing
# Prioritzem multi-cut vias
set_db route_design_detail_use_multi_cut_via_effort high


create_route_type -name ccopt -top_preferred_layer 6 -bottom_preferred_layer 4 
create_route_type -name ccopt_leaf -top_preferred_layer 5 -bottom_preferred_layer 3 
set_db cts_route_type ccopt
set_db cts_route_type_leaf ccopt_leaf
set_db cts_route_clock_tree_nets true
set_db cts_check_route_follows_guide true

commit_clock_tree_route_attributes

#cream arxiu amb les dades del ccopt seteades més amunt
create_clock_tree_spec -out_file CTS/ccopt_native.spec

#la veiem
#more ccopt_native.spec

#correm el CCOpt
source CTS/ccopt_native.spec
ccopt_design -report_dir CTS
write_db ./saved/cts_ccopt.invs
report_summary -out_file middle_reports/cts_ccopt.rpt -no_html

#visualitzam resultats del CTS (clock tree synthesis)
#obrir a gedit o qualsevol editor el log de innovus (innovus.log). A continuació cercar per "Initial Summary" i "optDesign Final Summary"
#a continuació generam reports
report_skew_groups -out_file CTS/ccopt_skew_groups.rpt
report_clock_trees -out_file CTS/ccopt_clock_trees.rpt
report_ccopt_worst_chain -out_file CTS/ccopt_worst_chain.rpt

# Al report de skew groups sha de mirar "Skew group structure", "skew group summary" i "skew group min/max path pins"
# Al report de clock trees mirar "slew", "area" i "buffer count" per cada clock tree
# Al report de worst chain mirar la worst chain pel disseny

#després obrim el CTD (Clock Tree Debugger), lab 2.5 p5, convé mirar com a quedat el clock tree, i quins son els millors i pitjors camins (min path, max path, transition time, timing window) etc.
#gui_open_ctd -id ctd_window

#es veu tota l'estructura del clock tree generat. també es poden obrir a la pestanya View, els panells de control, i del key.




#seguim amb el "Running Hold Tming Analysis after CTS" - Post-CTS Lab 2.5 p11
#carregam arxiu de constraints
source scripts/innvs_update_constraints.tcl

#anam a fer l'anàlisi de temps post-CTS per a setup (opcio default)
time_design -post_cts -report_dir CTS
#anam a fer l'anàlisi de temps post-CTS per a hold 
time_design -post_cts -hold -report_dir CTS

#seguim amb el "Running Timing Optimization to Fix Hold Time Violations". 
opt_design -post_cts -setup -hold -report_dir CTS
opt_design -post_cts -drv

#guardem el disseny a la carpeta innovus (final lab2.5 p.12)
write_db saved/postCTS.invs
report_summary -out_file middle_reports/postCTS.rpt -no_html

gui_redraw
gui_fit
